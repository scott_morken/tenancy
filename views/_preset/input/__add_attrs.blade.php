<?php
$add_attrs = $add_attrs ?? [];
?>
@foreach($add_attrs as $attr_key => $attr_value)
    @include('tenancy::_preset.input.__attr')
@endforeach
