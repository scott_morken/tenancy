<a @if (isset($id)) id="{{ $id }}" @endif
        @include('tenancy::_preset.input.__attrs', ['attrs' => ['href' => $href ?? '#', 'title' => $title_attr ?? ($title ?? ''), 'class' => $classes ?? '']])
        @include('tenancy::_preset.input.__attrs', ['attrs' => $add_attrs ?? []])
>
    {{ $title ?? 'link' }}
</a>
