<div class="mb-2 form-check {{ $wrapper_classes??'' }}">
    <input type="hidden" name="{{ $name }}" value="0">
    @include('tenancy::_preset.input._checkbox')
    @include('tenancy::_preset.input._label', ['id' => ($id ?? $name).'-'.($value ?? 1), 'label_classes' => $label_classes ?? 'form-check-label'])
</div>
