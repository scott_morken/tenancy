<?php
$attrs = $attrs ?? [];
?>
@foreach($attrs as $attr_key => $attr_value)
    @include('tenancy::_preset.input.__attr')
@endforeach
