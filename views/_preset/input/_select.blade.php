@php
    $name = $name ?? 'input-select-'.rand(0, 1000);
    $attrs = [
      'attrs' => [
          'class' => 'form-select '.($classes ?? ''),
          'name' => $name,
      ]
    ];
@endphp
<select @include('tenancy::_preset.input.__id')
        @include('tenancy::_preset.input.__attrs', $attrs)
        @include('tenancy::_preset.input.__attrs', ['attrs' => $add_attrs ?? []])
>
    <?php $value = $value ?? old($name, (isset($model) ? $model->$name : null)); ?>
    @foreach ($items as $k => $v)
        <option value="{{ $k }}" {{ (!is_null($value) && $value == $k) ? 'selected' : '' }}>{{ $v }}</option>
    @endforeach
</select>
