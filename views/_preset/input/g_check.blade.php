<div class="mb-2 form-check {{ $wrapper_classes??'' }}">
    @include('tenancy::_preset.input._checkbox')
    @include('tenancy::_preset.input._label', ['id' => ($id ?? $name).'-'.($value ?? 1), 'label_classes' => 'form-check-label'])
</div>
