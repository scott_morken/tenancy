<?php
$attr_name = $attr_name ?? 'id';
$id = $id ?? $name ?? 'input-'.rand(0, 1000);
$id = preg_replace('/[-]{2,}/', '-',
    \Illuminate\Support\Str::slug(str_replace(['[', ']', '_'], '-', $id)));
?>
@include('tenancy::_preset.input.__attr', ['attr_key' => $attr_name, 'attr_value' => $id])
