<?php
$params = array_merge([$model->getKeyName() => $model->getKey()], isset($filter) ? $filter->toArray() : []);
$first_col = 'col-sm-2';
$second_col = 'col-sm-10';
$limit_columns = $limit_columns ?? [];
?>
@section('content')
    @include('tenancy::_preset.controller._to_index')
    @include('tenancy::_preset.controller._title', ['title' => $title??''])
    <h5 class="mb-2">Delete record [{{ $model->getKey() }}]</h5>
    <div class="alert alert-danger">
        <div>Are you sure you want to delete this record?</div>
        @foreach ($model->attributesToArray() as $k => $v)
            @if (empty($limit_columns) || in_array($k, $limit_columns))
                <div class="row mb-2">
                    <div
                            class="{{ $first_col }} font-weight-bold fw-bold">{{ method_exists($model, 'friendlyColumn') ? $model->friendlyColumn($k) : $k }}</div>
                    <div class="{{ $second_col }}">
                        @if (is_array($v))
                            {{ \Smorken\Support\Arr::stringify($v) }}
                        @elseif (is_object($v))
                            (object)
                        @else
                            {{ $v }}
                        @endif
                    </div>
                </div>
            @endif
        @endforeach
    </div>
    <form method="POST">
        @method('DELETE')
        @csrf
        <div>
            <button type="submit" class="btn btn-danger">Delete</button>
            <a href="{{ action([$controller, 'index'], isset($filter)?$filter->toArray():[]) }}"
               title="Cancel" class="btn btn-outline-primary">Cancel</a>
        </div>
    </form>
@endsection
