<?php
/**
 * @var \Illuminate\Support\Collection $models
 */
$limit_columns = $limit_columns ?? [];
$extra = $extra ?? [];
?>
@section('content')
    @include('tenancy::_preset.controller._title', ['title' => $title??''])
    @includeIf($filter_form_view??'none')
    @include('tenancy::_preset.controller.index_actions._create')

    @if ($models && count($models))
        <?php $first = $models->first(); ?>
        <table class="table table-striped">
            <thead>
            <tr>
                @foreach ($first->getAttributes() as $k => $v)
                    @if (empty($limit_columns) || in_array($k, $limit_columns))
                        <th>{{ method_exists($first, 'friendlyColumn') ? $first->friendlyColumn($k) : $k }}</th>
                    @endif
                @endforeach
                @foreach ($extra as $k => $v)
                    <th>
                        {{ $k }}
                    </th>
                @endforeach
                <th>&nbsp;</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($models as $model)
                <?php $params = array_merge([$model->getKeyName() => $model->getKey()],
                    isset($filter) ? $filter->except(['page']) : []); ?>
                <tr id="row-for-{{ $model->getKey() }}">
                    @foreach ($model->getAttributes() as $k => $v)
                        @if (empty($limit_columns) || in_array($k, $limit_columns))
                            <td>
                                @if ($k === $model->getKeyName())
                                    @include('tenancy::_preset.controller.index_actions._view', ['value' => $v])
                                @else
                                    {{ $v }}
                                @endif
                            </td>
                        @endif
                    @endforeach
                    @foreach ($extra as $v)
                        <td>
                            {{ is_callable($v) ? $v($model) : $v }}
                        </td>
                    @endforeach
                    <td>
                        @include('tenancy::_preset.controller.index_actions._update_delete')
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        @if (method_exists($models, 'links'))
            {{ $models->appends(isset($filter)?$filter->except(['page']):[])->links() }}
        @endif
    @else
        <div class="text-muted">No records found.</div>
    @endif
@endsection
