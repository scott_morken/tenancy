<?php $filtered = 'border border-success'; ?>
<div class="card my-2">
    <div class="card-body">
        <form class="row g-3 align-items-center" method="get">
            <div class="col-md">
                @include('tenancy::_preset.input._label', ['name' => 'NAME', 'title' => 'NAME', 'label_classes' => 'sr-only'])
                @include('tenancy::_preset.input._input', [
                'name' => 'NAME',
                'classes' => $filter->NAME ? $filtered : '',
                'value' => $filter->NAME,
                'placeholder' => 'NAME'
                ])
            </div>
            <div class="col-md">
                @include('tenancy::_preset.input._label', ['name' => 'NAME_ID', 'title' => 'NAME_ID', 'label_classes' => 'sr-only'])
                @include('tenancy::_preset.input._select', [
                'name' => 'NAME_ID',
                'classes' => $filter->NAME_ID ? $filtered : '',
                'value' => $filter->NAME_ID,
                'items' => ['' => '-- Select One --'] + $NAME_IDS->pluck('descr', 'id')->all()
                ])
            </div>
            <div class="col-md">
                <button type="submit" class="btn btn-primary mb-2 mr-2">Filter</button>
                <a href="{{ action([$controller, 'index']) }}" class="btn btn-outline-danger mb-2" title="Reset filter">Reset</a>
            </div>
        </form>
    </div>
</div>
