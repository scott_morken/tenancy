@extends(\Illuminate\Support\Facades\Config::get('tenancyext.admin.layout', 'layouts.app'))
@include('tenancy::_preset.controller.update', ['title' => 'Tenant Administration', 'inputs_view' => 'tenancy::admin.tenant._form'])
