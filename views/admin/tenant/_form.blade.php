<?php
$creating = $creating ?? false;
?>
@if ($creating)
    @include('tenancy::_preset.input.g_input', ['name' => 'id', 'title' => 'ID'])
@else
    <div class="mb-3"><span class="">ID:</span> <span>{{ $model->getTenantKey() }}</span></div>
@endif
@include('tenancy::_preset.input.g_input', ['name' => 'name', 'title' => 'Name'])
@include('tenancy::_preset.input.g_check_bool', ['name' => 'active', 'title' => 'Active'])
<div class="card">
    <div class="card-header">Domains</div>
    <div class="card-body">
        <div class="mb-2">
            @include('tenancy::_preset.input._input', ['name' => 'domains[]', 'value' => old('domains.0')])
        </div>
        @if ($model)
            @foreach ($model->domains as $domain)
                <div class="mb-2">
                    @include('tenancy::_preset.input._input', ['name' => 'domains[]', 'value' => $domain->domain])
                </div>
            @endforeach
        @endif
    </div>
</div>
