@extends(\Illuminate\Support\Facades\Config::get('tenancyext.admin.layout', 'layouts.app'))
@include('tenancy::_preset.controller.view', [
    'title' => 'Tenant Administration',
    'extra' => [
        'Domains' => (new \Smorken\Tenancy\View\Helpers\DomainHelper($model))->toString()
    ]
])
