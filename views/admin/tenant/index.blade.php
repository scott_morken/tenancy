<?php
/**
 * @var \Illuminate\Support\Collection $models
 * @var \Smorken\Tenancy\Contracts\Models\Tenant $model
 * @var \Smorken\Support\Contracts\Filter $filter
 */
?>
@extends(\Illuminate\Support\Facades\Config::get('tenancyext.admin.layout', 'layouts.app'))
@include('tenancy::_preset.controller.index', [
    'title' => 'Tenant Administration',
    'filter_form_view' => 'tenancy::admin.tenant._filter_form',
    'limit_columns' => ['id', 'name', 'active'],
    'extra' => [
        'Domains' => fn ($m) => (new \Smorken\Tenancy\View\Helpers\DomainHelper($m))->toString()
    ]
])
