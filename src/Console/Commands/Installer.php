<?php

namespace Smorken\Tenancy\Console\Commands;

use Illuminate\Console\Command;
use Smorken\Tenancy\ServiceProvider;

class Installer extends Command
{

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Install smorken/tenancy.';

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'tenancyext:install';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(): int
    {
        $this->comment('Installing smorken/tenancy...');
        $this->callSilent('vendor:publish', [
            '--provider' => ServiceProvider::class,
            '--tag' => 'config',
        ]);
        $this->info(' > Created config/tenancyext.php');

        if (!is_dir(database_path('migrations/tenant'))) {
            mkdir(database_path('migrations/tenant'));
            $this->info(' > Created database/migrations/tenant folder.');
        }

        $this->comment('smorken/tenancy installed successfully.');
        return 0;
    }
}
