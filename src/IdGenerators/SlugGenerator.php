<?php

namespace Smorken\Tenancy\IdGenerators;

use Illuminate\Support\Str;
use Stancl\Tenancy\Contracts\UniqueIdentifierGenerator;

class SlugGenerator implements UniqueIdentifierGenerator
{

    public static function generate($resource): string
    {
        return Str::slug($resource->id, '_');
    }
}
