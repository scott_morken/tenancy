<?php

namespace Smorken\Tenancy\Routes;

use Illuminate\Routing\Router;
use Smorken\Support\LoadRoutes;
use Smorken\Tenancy\Controllers\Asset\Controller;

class AssetRoutes extends LoadRoutes
{

    protected function loadRoutes(Router $router): void
    {
        $controller = $this->app['config']->get('tenancyext.assets.controller', Controller::class);
        $router->get('/tenancy/assets/{path?}', $controller)
               ->where('path', '(.*)');
    }
}
