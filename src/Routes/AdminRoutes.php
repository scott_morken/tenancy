<?php

namespace Smorken\Tenancy\Routes;

use Illuminate\Routing\Router;
use Smorken\Support\LoadRoutes;

class AdminRoutes extends LoadRoutes
{

    protected function loadRoutes(Router $router): void
    {
        \Smorken\Support\Routes::create(\Smorken\Tenancy\Controllers\Admin\Tenant\Controller::class);
    }
}
