<?php

namespace Smorken\Tenancy;

class Paths implements \Smorken\Tenancy\Contracts\Paths
{

    public function __construct(protected string $basePath)
    {
    }

    public function basePath(string $path = ''): string
    {
        return $this->basePath.($path != '' ? DIRECTORY_SEPARATOR.$path : '');
    }

    public function configPath(string $path = ''): string
    {
        return $this->basePath.DIRECTORY_SEPARATOR.'config'.($path != '' ? DIRECTORY_SEPARATOR.$path : '');
    }

    public function resourcePath(string $path = ''): string
    {
        return $this->basePath.DIRECTORY_SEPARATOR.'resources'.($path != '' ? DIRECTORY_SEPARATOR.$path : '');
    }
}
