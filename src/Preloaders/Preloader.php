<?php

namespace Smorken\Tenancy\Preloaders;

use Stancl\Tenancy\Contracts\TenantResolver;

abstract class Preloader implements \Smorken\Tenancy\Contracts\Preloaders\Preloader
{

    protected TenantResolver $resolver;

    public function getResolver(): TenantResolver
    {
        return $this->resolver;
    }
}
