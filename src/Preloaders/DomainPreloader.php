<?php

namespace Smorken\Tenancy\Preloaders;

use Illuminate\Http\Request;
use Stancl\Tenancy\Contracts\Tenant;
use Stancl\Tenancy\Contracts\TenantCouldNotBeIdentifiedException;
use Stancl\Tenancy\Resolvers\DomainTenantResolver;

class DomainPreloader extends Preloader
{

    public function __construct(DomainTenantResolver $resolver)
    {
        $this->resolver = $resolver;
    }

    public function load(Request $request): ?Tenant
    {
        try {
            return $this->getResolver()->resolve($request->getHost());
        } catch (TenantCouldNotBeIdentifiedException) {
            return null;
        }
    }
}
