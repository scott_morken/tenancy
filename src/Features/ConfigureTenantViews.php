<?php

namespace Smorken\Tenancy\Features;

use Illuminate\Contracts\Config\Repository;
use Illuminate\Contracts\View\Factory;
use Illuminate\Support\Facades\Event;
use Smorken\Tenancy\Contracts\Models\Tenant;
use Smorken\Tenancy\Contracts\Paths;
use Stancl\Tenancy\Contracts\Feature;
use Stancl\Tenancy\Events\RevertedToCentralContext;
use Stancl\Tenancy\Events\TenancyBootstrapped;
use Stancl\Tenancy\Tenancy;

class ConfigureTenantViews implements Feature
{

    public static bool $removeOriginalViewPaths = false;

    protected array $originalConfigPaths = [];

    protected array $originalViewPaths = [];

    public function __construct(
        protected Repository $config,
        protected Factory $viewFactory,
        protected Paths $paths
    ) {
    }

    public function bootstrap(Tenancy $tenancy): void
    {
        Event::listen(TenancyBootstrapped::class, function (TenancyBootstrapped $event) {
            $this->setTenantViews($event->tenancy->tenant);
        });

        Event::listen(RevertedToCentralContext::class, function () {
            $this->setViewPaths($this->originalConfigPaths, true);
        });
    }

    protected function getConfig(): Repository
    {
        return $this->config;
    }

    protected function getCurrentViewPathsFromFinder(): array
    {
        return $this->getView()->getFinder()->getPaths();
    }

    protected function getPaths(): Paths
    {
        return $this->paths;
    }

    protected function getTenantOwnViewPath(Tenant $tenant): string|false
    {
        return realpath($this->getPaths()->resourcePath('tenant_'.$tenant->getTenantKey().DIRECTORY_SEPARATOR.'views'));
    }

    protected function getTenantViews(Tenant $tenant): array
    {
        $views = [];
        if ($this->tenantHasOwnViews($tenant)) {
            $views[] = $this->getTenantOwnViewPath($tenant);
        }
        $views[] = realpath($this->getPaths()->resourcePath('core/views'));
        return $views;
    }

    protected function getView(): Factory
    {
        return $this->viewFactory;
    }

    protected function removePathsFromArray(array $paths, array $remove): array
    {
        $parsed = [];
        foreach ($paths as $path) {
            if (!in_array($path, $remove)) {
                $parsed[] = $path;
            }
        }
        return $parsed;
    }

    protected function setTenantViews(Tenant $tenant): void
    {
        $this->storeCurrent();
        $views = $this->getTenantViews($tenant);
        $this->setViewPaths($views);
    }

    protected function setViewPaths(array $paths, bool $forceRemoval = false): void
    {
        if ($paths) {
            $original = $this->getConfig()->get('view.paths');
            $this->getConfig()->set('view.paths', $paths);
            $this->setViewPathsInFinder($paths, $original, $forceRemoval);
        }
    }

    protected function setViewPathsInFinder(array $newPaths, array $removePaths, bool $forceRemoval = false): void
    {
        $currentPaths = $this->getCurrentViewPathsFromFinder();
        if (self::$removeOriginalViewPaths || $forceRemoval) {
            $currentPaths = $this->removePathsFromArray($currentPaths, $removePaths);
        }
        $this->getView()->getFinder()->setPaths(array_unique([...$newPaths, ...$currentPaths]));
    }

    protected function storeCurrent(): void
    {
        $this->originalConfigPaths = $this->getConfig()->get('view.paths');
        $this->originalViewPaths = $this->getCurrentViewPathsFromFinder();
    }

    protected function tenantHasOwnViews(Tenant $tenant): bool
    {
        return $this->getTenantOwnViewPath($tenant) !== false;
    }
}
