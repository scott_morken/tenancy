<?php

namespace Smorken\Tenancy\Features;

use Illuminate\Contracts\Config\Repository;
use Illuminate\Support\Facades\Event;
use Smorken\Tenancy\Contracts\Config\Loader;
use Smorken\Tenancy\Contracts\Models\Tenant;
use Stancl\Tenancy\Contracts\Feature;
use Stancl\Tenancy\Events\RevertedToCentralContext;
use Stancl\Tenancy\Events\TenancyBootstrapped;
use Stancl\Tenancy\Tenancy;

class MergeTenantConfig implements Feature
{

    protected array $originalConfig = [];

    public function __construct(protected Loader $loader)
    {
    }

    public function bootstrap(Tenancy $tenancy): void
    {
        Event::listen(TenancyBootstrapped::class, function (TenancyBootstrapped $event) {
            $this->setTenantConfig($event->tenancy->tenant);
        });

        Event::listen(RevertedToCentralContext::class, function () {
            $this->unsetTenantConfig();
        });
    }

    public function getConfig(): Repository
    {
        return $this->loader->getConfig();
    }

    public function getLoader(): Loader
    {
        return $this->loader;
    }

    public function setTenantConfig(Tenant $tenant): void
    {
        $this->originalConfig = $this->getConfig()->all();
        $this->getLoader()->bootstrap($tenant);
    }

    public function unsetTenantConfig(): void
    {
        foreach ($this->originalConfig as $key => $value) {
            $this->getConfig()->set($key, $value);
        }
    }
}
