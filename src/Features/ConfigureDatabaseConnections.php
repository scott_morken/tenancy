<?php

namespace Smorken\Tenancy\Features;

use Illuminate\Contracts\Config\Repository;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Database\Connectors\ConnectionFactory;
use Illuminate\Database\DatabaseManager;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Event;
use Smorken\Tenancy\Contracts\Models\Tenant;
use Stancl\Tenancy\Contracts\Feature;
use Stancl\Tenancy\Events\RevertedToCentralContext;
use Stancl\Tenancy\Events\TenancyBootstrapped;
use Stancl\Tenancy\Tenancy;

class ConfigureDatabaseConnections implements Feature
{

    protected array $instances = [];

    protected array $originalConfig = [];

    public function __construct(protected Application $application)
    {
    }

    public function bootstrap(Tenancy $tenancy): void
    {
        Event::listen(TenancyBootstrapped::class, function (TenancyBootstrapped $event) {
            $this->configureDatabaseConnections($event->tenancy->tenant);
        });

        Event::listen(RevertedToCentralContext::class, function () {
            $this->resetDatabaseConnections();
        });
    }

    public function getApplication(): Application
    {
        return $this->application;
    }

    public function getConfigRepository(): Repository
    {
        return $this->make('config');
    }

    public function getConnectionFactory(): ConnectionFactory
    {
        return $this->make('db.factory');
    }

    public function getDbManager(): DatabaseManager
    {
        return $this->make('db');
    }

    protected function cleanup(string $connectionName): void
    {
        $this->getDbManager()->purge($connectionName);

        // Octane will have an old `db` instance in the Model::$resolver.
        Model::setConnectionResolver($this->getDbManager());
    }

    protected function configureDatabaseConnection(Tenant $tenant, string $connectionName, ?array $configMap): void
    {
        $currentConfig = $this->getCurrentConnectionConfig($connectionName);
        $this->originalConfig[$connectionName] = $currentConfig;
        $newConfig = $this->createDatabaseConfig($tenant, $connectionName, $configMap ?? []);
        $this->pushConfig($connectionName, $currentConfig, $newConfig);
        $this->cleanup($connectionName);
    }

    protected function configureDatabaseConnections(Tenant $tenant): void
    {
        foreach ($this->getConnectionConfigs() as $connectionName => $configMap) {
            $this->configureDatabaseConnection($tenant, $connectionName, $configMap);
        }
    }

    protected function createDatabaseConfig(Tenant $tenant, string $connection, array $configMap): array
    {
        return (new \Smorken\Tenancy\Features\Helpers\TenantDatabaseConfig($connection, $configMap))->create($tenant);
    }

    protected function getConnectionConfigKey(string $connectionName): string
    {
        return "database.connections.{$connectionName}";
    }

    protected function getConnectionConfigs(): array
    {
        return $this->getConfigRepository()->get('tenancyext.connections', []);
    }

    protected function getCurrentConnectionConfig(string $connectionName): array
    {
        return $this->getConfigRepository()->get($this->getConnectionConfigKey($connectionName), []);
    }

    protected function make(string $what): mixed
    {
        if (!($this->instances[$what] ?? false)) {
            $this->instances[$what] = $this->getApplication()->make($what);
        }
        return $this->instances[$what];
    }

    protected function pushConfig(
        string $connectionName,
        array $currentConfig,
        array $newConfig,
        bool $useNulls = false
    ): void {
        foreach ($newConfig as $key => $value) {
            if ($value !== null || $useNulls) {
                $currentConfig[$key] = $value;
            }
        }
        $this->getConfigRepository()->set($this->getConnectionConfigKey($connectionName), $currentConfig);
        $this->getDbManager()->extend($connectionName, function (array $config, string $name) use ($currentConfig) {
            $config = array_merge($config, $currentConfig);
            return $this->getConnectionFactory()->make($config, $name);
        });
    }

    protected function resetDatabaseConnection(string $connectionName): void
    {
        $this->verifyConnectionConfigExists($connectionName);
        $this->pushConfig($connectionName, $this->getCurrentConnectionConfig($connectionName),
            $this->originalConfig[$connectionName] ?? [], true);
        $this->cleanup($connectionName);
    }

    protected function resetDatabaseConnections(): void
    {
        foreach ($this->getConnectionConfigs() as $connectionName => $configMap) {
            $this->resetDatabaseConnection($connectionName);
        }
    }

    protected function verifyConnectionConfigExists(string $connectionName): void
    {
        if (is_null($this->getConfigRepository()->get($this->getConnectionConfigKey($connectionName)))) {
            throw new \OutOfBoundsException($connectionName.' does not exist.');
        }
    }
}
