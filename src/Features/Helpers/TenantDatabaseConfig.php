<?php

namespace Smorken\Tenancy\Features\Helpers;

use Illuminate\Support\Env;
use Smorken\Tenancy\Contracts\Models\Tenant;

class TenantDatabaseConfig implements \Smorken\Tenancy\Contracts\Features\Helpers\TenantDatabaseConfig
{

    protected array $configMap = [
        'HOST' => 'host',
        'DATABASE' => 'database',
        'USERNAME' => 'username',
        'PASSWORD' => 'password',
    ];

    public function __construct(protected string $connectionName = 'tenant', array $configMap = [])
    {
        if ($configMap) {
            $this->configMap = $configMap;
        }
    }

    public function create(Tenant $tenant): array
    {
        $config = [];
        foreach ($this->getConfigMap() as $envSuffix => $configKey) {
            $envKey = $this->getEnvKey($tenant, $envSuffix);
            $config[$configKey] = $this->valueFromEnv($envKey);
        }
        return $config;
    }

    public function getConfigMap(): array
    {
        return $this->configMap;
    }

    public function getConnectionName(): string
    {
        return $this->connectionName;
    }

    public function getEnvKey(Tenant $tenant, string $suffix): string
    {
        return strtoupper(implode('_', [$this->getConnectionName(), $tenant->getTenantKey(), $suffix]));
    }

    protected function valueFromEnv(string $key): ?string
    {
        return Env::getRepository()->get($key);
    }
}
