<?php

namespace Smorken\Tenancy\Controllers\Admin\Tenant;

use Smorken\Controller\View\WithService\CrudController;
use Smorken\Tenancy\Contracts\Services\Tenants\Admin\CrudServices;
use Smorken\Tenancy\Contracts\Services\Tenants\Admin\IndexService;

class Controller extends CrudController
{

    protected string $baseView = 'tenancy::admin.tenant';

    public function __construct(CrudServices $crudServices, IndexService $indexService)
    {
        parent::__construct($crudServices, $indexService);
    }
}
