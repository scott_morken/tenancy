<?php

namespace Smorken\Tenancy\Controllers\Asset;

use Smorken\Tenancy\Contracts\Services\Tenants\AssetService;
use Stancl\Tenancy\Contracts\Tenant;
use Stancl\Tenancy\Tenancy;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class Controller extends \Smorken\Controller\View\Controller
{

    public function __construct(protected AssetService $service, protected Tenancy $tenancy)
    {
        parent::__construct();
    }

    public function __invoke(?string $path = null): BinaryFileResponse
    {
        if ($path === null || $this->getTenant() === null) {
            throw new NotFoundHttpException();
        }
        try {
            $filePath = $this->service->path($this->getTenant(), $path);
            if ($filePath) {
                return \Illuminate\Support\Facades\Response::file($filePath);
            }
        } catch (\Throwable) {
            // continue to throw 404
        }
        throw new NotFoundHttpException();
    }

    protected function getTenant(): ?Tenant
    {
        return $this->tenancy->tenant;
    }
}
