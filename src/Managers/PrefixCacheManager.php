<?php

namespace Smorken\Tenancy\Managers;

use Illuminate\Cache\CacheManager;
use Stancl\Tenancy\Contracts\Tenant;

class PrefixCacheManager extends CacheManager
{

    protected function getPrefix(array $config)
    {
        return implode('_', array_filter([
            ...$this->getTenantPrefixParts(),
            parent::getPrefix($config),
        ]));
    }

    protected function getTenantKey(): string
    {
        return $this->app[Tenant::class]->getTenantKey();
    }

    protected function getTenantPrefixParts(): array
    {
        return [
            $this->app['config']->get('tenancyext.cache_prefix'),
            $this->getTenantKey(),
        ];
    }
}
