<?php

namespace Smorken\Tenancy\Contracts\Features\Helpers;

use Smorken\Tenancy\Contracts\Models\Tenant;

interface TenantDatabaseConfig
{

    public function create(Tenant $tenant): array;

    public function getConfigMap(): array;

    public function getConnectionName(): string;

    public function getEnvKey(Tenant $tenant, string $suffix): string;
}
