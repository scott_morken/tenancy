<?php

namespace Smorken\Tenancy\Contracts\Models;

use Smorken\Model\Contracts\Model;
use Stancl\Tenancy\Contracts\TenantWithDatabase;

/**
 * @property int $id
 * @property string $name
 * @property boolean $active
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 */
interface Tenant extends Model, \Stancl\Tenancy\Contracts\Tenant, TenantWithDatabase
{

}
