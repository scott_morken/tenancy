<?php

namespace Smorken\Tenancy\Contracts\Preloaders;

use Illuminate\Http\Request;
use Stancl\Tenancy\Contracts\Tenant;
use Stancl\Tenancy\Contracts\TenantResolver;

interface Preloader
{

    public function getResolver(): TenantResolver;

    public function load(Request $request): ?Tenant;
}
