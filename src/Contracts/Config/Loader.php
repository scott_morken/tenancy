<?php

namespace Smorken\Tenancy\Contracts\Config;

use Illuminate\Contracts\Config\Repository;
use Smorken\Tenancy\Contracts\Models\Tenant;
use Smorken\Tenancy\Contracts\Paths;

interface Loader
{

    public function bootstrap(Tenant $tenant): void;

    public function getConfig(): Repository;

    public function getPaths(): Paths;
}
