<?php

namespace Smorken\Tenancy\Contracts;

interface Paths
{

    public function basePath(string $path = ''): string;

    public function configPath(string $path = ''): string;

    public function resourcePath(string $path = ''): string;
}
