<?php

namespace Smorken\Tenancy\Contracts\Services\Tenants;

use Smorken\Service\Contracts\Services\BaseService;
use Smorken\Service\Contracts\Services\VO\ModelResult;
use Smorken\Tenancy\Contracts\Storage\Tenant;

interface RetrieveService extends BaseService
{

    public function current(): ModelResult;

    public function getProvider(): Tenant;
}
