<?php

namespace Smorken\Tenancy\Contracts\Services\Tenants;

use Smorken\Service\Contracts\Services\BaseService;
use Smorken\Tenancy\Contracts\Paths;
use Stancl\Tenancy\Contracts\Tenant;

interface AssetService extends BaseService
{

    public function getPaths(): Paths;

    public function path(Tenant $tenant, string $path): string|false;
}
