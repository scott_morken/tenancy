<?php

namespace Smorken\Tenancy\Contracts\Services\Tenants;

use Smorken\Service\Contracts\Services\BaseService;
use Smorken\Service\Contracts\Services\VO\CollectionResult;
use Smorken\Tenancy\Contracts\Storage\Tenant;

interface CollectionService extends BaseService
{

    public function active(): CollectionResult;

    public function all(): CollectionResult;

    public function getProvider(): Tenant;
}
