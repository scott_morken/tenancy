<?php

namespace Smorken\Tenancy\Contracts\Services\Tenants\Admin;

use Smorken\Service\Contracts\Services\HasFilterService;

interface IndexService extends \Smorken\Service\Contracts\Services\IndexService, HasFilterService
{

}
