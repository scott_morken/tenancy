<?php

namespace Smorken\Tenancy\Contracts\Storage;

use Illuminate\Support\Collection;
use Smorken\Storage\Contracts\Base;

interface Tenant extends Base
{

    public function active(): Collection;

    public function current(): \Smorken\Tenancy\Contracts\Models\Tenant|null;

    public function syncDomains(
        \Smorken\Tenancy\Contracts\Models\Tenant $tenant,
        array $domains
    ): \Smorken\Tenancy\Contracts\Models\Tenant;
}
