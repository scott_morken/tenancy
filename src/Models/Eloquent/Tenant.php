<?php

namespace Smorken\Tenancy\Models\Eloquent;

use Illuminate\Contracts\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Validation\Rule;
use Smorken\Model\Concerns\WithFriendlyKeys;
use Stancl\Tenancy\Database\Concerns\HasDatabase;
use Stancl\Tenancy\Database\Concerns\HasDomains;

class Tenant extends \Stancl\Tenancy\Database\Models\Tenant implements \Smorken\Tenancy\Contracts\Models\Tenant
{

    use WithFriendlyKeys, HasDomains, HasDatabase, HasFactory;

    public static function getCustomColumns(): array
    {
        return [
            'id',
            'active',
            'created_at',
            'updated_at',
        ];
    }

    public function scopeActiveIs(Builder $query, int $active = 1): Builder
    {
        return $query->where('active', '=', $active);
    }

    public function scopeDefaultOrder(Builder $query): Builder
    {
        return $query->orderBy('id');
    }

    public function scopeDefaultWiths(Builder $query): Builder
    {
        return $query->with('domains');
    }

    public function setAttributes(iterable $attributes): void
    {
        foreach ($attributes as $key => $value) {
            $this->setAttribute($key, $value);
        }
    }
}
