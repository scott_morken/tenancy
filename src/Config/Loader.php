<?php

namespace Smorken\Tenancy\Config;

use Illuminate\Contracts\Config\Repository;
use Smorken\Tenancy\Contracts\Models\Tenant;
use Smorken\Tenancy\Contracts\Paths;
use Symfony\Component\Finder\Finder;

class Loader implements \Smorken\Tenancy\Contracts\Config\Loader
{

    public function __construct(protected Repository $config, protected Paths $paths, protected $overwriteKeys = [])
    {
    }

    public function bootstrap(Tenant $tenant): void
    {
        $this->bootstrapCore();
        $this->bootstrapTenant($tenant);
    }

    public function getConfig(): Repository
    {
        return $this->config;
    }

    public function getPaths(): Paths
    {
        return $this->paths;
    }

    protected function bootstrapCore(): void
    {
        $files = $this->getCoreConfigFiles();
        $this->pushFilesToConfig($files);
    }

    protected function bootstrapTenant(Tenant $tenant): void
    {
        $files = $this->getTenantConfigFiles($tenant);
        $this->pushFilesToConfig($files);
    }

    protected function getConfigData(string $key, string $path): mixed
    {
        $current = $this->getConfig()->get($key) ?? [];
        $fromPath = require $path;
        if (in_array($key, $this->overwriteKeys)) {
            $data = $fromPath;
        } else {
            $data = array_replace_recursive($current, $fromPath);
        }
        return $data;
    }

    protected function getConfigFilesFromPath(string $path): array
    {
        $files = [];
        foreach (Finder::create()->files()->name('*.php')->in($path) as $file) {
            $files[basename($file->getRealPath(), '.php')] = $file->getRealPath();
        }
        return $files;
    }

    protected function getCoreConfigFiles(): array
    {
        $path = realpath($this->getPaths()->configPath('core'));
        return $this->getConfigFilesFromPath($path);
    }

    protected function getTenantConfigFiles(Tenant $tenant): array
    {
        $path = realpath($this->getPaths()->configPath(sprintf('tenant_%s', $tenant->getTenantKey())));
        if ($path === false) {
            return [];
        }
        return $this->getConfigFilesFromPath($path);
    }

    protected function pushFilesToConfig(array $files): void
    {
        foreach ($files as $key => $path) {
            $this->getConfig()->set($key, $this->getConfigData($key, $path));
        }
    }
}
