<?php

namespace Smorken\Tenancy\Bootstrappers;

use Illuminate\Cache\CacheManager;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Support\Facades\Cache;
use Smorken\CacheAssist\CacheAssist;
use Smorken\Tenancy\Managers\PrefixCacheManager;
use Stancl\Tenancy\Contracts\TenancyBootstrapper;
use Stancl\Tenancy\Contracts\Tenant;

class PrefixCacheBootstrapper implements TenancyBootstrapper
{

    protected ?CacheManager $originalCache = null;

    public function __construct(protected Application $app)
    {
    }

    public function bootstrap(Tenant $tenant)
    {
        $this->resetFacadeCache();

        $this->originalCache = $this->originalCache ?? $this->app['cache'];
        $this->app->extend('cache', function () {
            return new PrefixCacheManager($this->app);
        });
        if (class_exists(CacheAssist::class)) {
            CacheAssist::setCache($this->app['cache']);
        }
    }

    /**
     * This wouldn't be necessary, but is needed when a call to the
     * facade has been made prior to bootstrapping tenancy. The
     * facade has its own cache, separate from the container.
     */
    public function resetFacadeCache(): void
    {
        Cache::clearResolvedInstances();
    }

    public function revert()
    {
        $this->resetFacadeCache();

        $this->app->extend('cache', function () {
            return $this->originalCache;
        });
        if (class_exists(CacheAssist::class) && $this->originalCache) {
            CacheAssist::setCache($this->originalCache);
        }
        $this->originalCache = null;
    }
}
