<?php

namespace Smorken\Tenancy\Bootstrappers;

use Illuminate\Support\Facades\Storage;
use Smorken\Tenancy\Controllers\Asset\Controller;
use Stancl\Tenancy\Bootstrappers\FilesystemTenancyBootstrapper;
use Stancl\Tenancy\Contracts\Tenant;

class FilesystemBootstrapper extends FilesystemTenancyBootstrapper
{

    public function bootstrap(Tenant $tenant)
    {
        $suffix = $this->app['config']['tenancy.filesystem.suffix_base'].$tenant->getTenantKey();
        $this->bootstrapStoragePath($tenant, $suffix);
        $this->bootstrapAsset($tenant, $suffix);
        $this->bootstrapStorageFacade($tenant, $suffix);
    }

    protected function bootstrapAsset(Tenant $tenant, string $suffix): void
    {
        if ($this->app['config']['tenancy.filesystem.asset_helper_tenancy'] ?? true) {
            if ($this->originalPaths['asset_url']) {
                $this->app['config']['app.asset_url'] = ($this->originalPaths['asset_url'] ?? $this->app['config']['app.url'])."/$suffix";
                $this->app['url']->setAssetRoot($this->app['config']['app.asset_url']);
            } else {
                $controller = $this->app['config']->get('tenancyext.assets.controller', Controller::class);
                $this->app['url']->setAssetRoot($this->app['url']->action($controller, ['path' => '']));
            }
        }
    }

    protected function bootstrapStorageFacade(Tenant $tenant, string $suffix): void
    {
        Storage::forgetDisk($this->app['config']['tenancy.filesystem.disks']);

        foreach ($this->app['config']['tenancy.filesystem.disks'] as $disk) {
            $originalRoot = $this->app['config']["filesystems.disks.{$disk}.root"];
            $this->originalPaths['disks'][$disk] = $originalRoot;

            $finalPrefix = str_replace(
                ['%storage_path%', '%tenant%'],
                [storage_path(), $tenant->getTenantKey()],
                $this->app['config']["tenancy.filesystem.root_override.{$disk}"] ?? '',
            );

            if (!$finalPrefix) {
                $finalPrefix = $originalRoot
                    ? rtrim($originalRoot, '/').'/'.$suffix
                    : $suffix;
            }

            $this->app['config']["filesystems.disks.{$disk}.root"] = $finalPrefix;
        }
    }

    protected function bootstrapStoragePath(Tenant $tenant, string $suffix): void
    {
        if ($this->app['config']['tenancy.filesystem.suffix_storage_path'] ?? true) {
            $this->app->useStoragePath($this->originalPaths['storage']."/{$suffix}");
        }
    }
}
