<?php

namespace Smorken\Tenancy\View\Helpers;

use Smorken\Tenancy\Contracts\Models\Tenant;

class DomainHelper
{

    public function __construct(protected Tenant $tenant)
    {
    }

    public function toString(): string
    {
        return implode(';', $this->tenant->domains->pluck('domain')->toArray()) ?: '--';
    }
}
