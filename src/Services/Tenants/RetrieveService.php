<?php

namespace Smorken\Tenancy\Services\Tenants;

use Smorken\Service\Services\BaseService;
use Smorken\Service\Services\VO\ModelResult;
use Smorken\Tenancy\Contracts\Storage\Tenant;

class RetrieveService extends BaseService implements \Smorken\Tenancy\Contracts\Services\Tenants\RetrieveService
{

    public function __construct(protected Tenant $provider, array $services = [])
    {
        parent::__construct($services);
    }

    public function current(): \Smorken\Service\Contracts\Services\VO\ModelResult
    {
        $model = $this->getProvider()->current();
        return new ModelResult($model, $model?->getKey(), (bool) $model);
    }

    public function getProvider(): Tenant
    {
        return $this->provider;
    }
}
