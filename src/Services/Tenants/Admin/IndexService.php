<?php

namespace Smorken\Tenancy\Services\Tenants\Admin;

use Smorken\Service\Services\PaginateByFilterService;

class IndexService extends PaginateByFilterService implements
    \Smorken\Tenancy\Contracts\Services\Tenants\Admin\IndexService
{

}
