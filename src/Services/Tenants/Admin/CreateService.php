<?php

namespace Smorken\Tenancy\Services\Tenants\Admin;

use Illuminate\Http\Request;
use Smorken\Model\Contracts\Model;
use Smorken\Service\Services\CreateByStorageProviderService;

class CreateService extends CreateByStorageProviderService
{

    public function getAttributesFromRequest(Request $request): array
    {
        return $request->only(['id', 'name', 'active']);
    }

    /**
     * @param  \Smorken\Model\Contracts\Model|\Smorken\Tenancy\Contracts\Models\Tenant|null  $model
     * @param  array  $attributes
     * @return \Smorken\Model\Contracts\Model|null
     */
    protected function postCreate(?Model $model, array $attributes): ?Model
    {
        if ($model) {
            $model = $this->getProvider()->syncDomains($model, $this->getRequest()->input('domains') ?? []);
        }
        return $model;
    }
}
