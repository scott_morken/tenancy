<?php

namespace Smorken\Tenancy\Services\Tenants\Admin;

use Smorken\Service\Services\CrudByStorageProviderServices;

class CrudServices extends CrudByStorageProviderServices implements
    \Smorken\Tenancy\Contracts\Services\Tenants\Admin\CrudServices
{

}
