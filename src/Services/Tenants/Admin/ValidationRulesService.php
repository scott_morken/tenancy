<?php

namespace Smorken\Tenancy\Services\Tenants\Admin;

use Illuminate\Http\Request;
use Smorken\Service\Services\ValidationRulesByStorageProviderService;

class ValidationRulesService extends ValidationRulesByStorageProviderService
{

    protected function modifyRulesForUpdate(array $rules, ?Request $request): array
    {
        unset($rules['id']);
        return $rules;
    }
}
