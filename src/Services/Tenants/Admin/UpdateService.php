<?php

namespace Smorken\Tenancy\Services\Tenants\Admin;

use Illuminate\Http\Request;
use Smorken\Model\Contracts\Model;
use Smorken\Service\Services\UpdateByStorageProviderService;

class UpdateService extends UpdateByStorageProviderService
{

    public function getAttributesFromRequest(Request $request): array
    {
        return $request->only(['name', 'active']);
    }

    protected function postUpdate(Model $model, array $attributes): Model
    {
        $this->getProvider()->syncDomains($model, $this->getRequest()->input('domains') ?? []);
        return $model;
    }
}
