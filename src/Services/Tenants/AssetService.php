<?php

namespace Smorken\Tenancy\Services\Tenants;

use Illuminate\Support\Str;
use Smorken\Service\Services\BaseService;
use Smorken\Tenancy\Contracts\Paths;
use Stancl\Tenancy\Contracts\Tenant;

class AssetService extends BaseService implements \Smorken\Tenancy\Contracts\Services\Tenants\AssetService
{

    public function __construct(protected Paths $paths, array $services = [])
    {
        parent::__construct($services);
    }

    public function getPaths(): Paths
    {
        return $this->paths;
    }

    public function path(Tenant $tenant, string $path): string|false
    {
        return $this->getPathForTenant($tenant, $path) ?: $this->getPathForCore($path);
    }

    protected function createPath(string $base, string $path, ?Tenant $tenant): string|false
    {
        $fullPath = realpath($base.Str::start($path, DIRECTORY_SEPARATOR));
        return $this->ensurePathIsResource($fullPath, $tenant);
    }

    protected function ensurePathIsResource(string|false $path, ?Tenant $tenant): string|false
    {
        if ($path) {
            $ok = false;
            if ($tenant) {
                $ok = str_starts_with($path, $this->getTenantOwnAssetPath($tenant));
            }
            if (!$ok) {
                $ok = str_starts_with($path, $this->getCoreAssetPath());
            }
            return $ok ? $path : false;
        }
        return false;
    }

    protected function getCoreAssetPath(): string|false
    {
        return realpath($this->getPaths()->resourcePath('core'.DIRECTORY_SEPARATOR.'assets'));
    }

    protected function getPathForCore(string $path): string|false
    {
        $coreBase = $this->getCoreAssetPath();
        if ($coreBase) {
            return $this->createPath($coreBase, $path, null);
        }
        return false;
    }

    protected function getPathForTenant(Tenant $tenant, string $path): string|false
    {
        $tenantBase = $this->getTenantOwnAssetPath($tenant);
        if ($tenantBase) {
            return $this->createPath($tenantBase, $path, $tenant);
        }
        return false;
    }

    protected function getTenantOwnAssetPath(Tenant $tenant): string|false
    {
        return realpath($this->getPaths()
                             ->resourcePath('tenant_'.$tenant->getTenantKey().DIRECTORY_SEPARATOR.'assets'));
    }
}
