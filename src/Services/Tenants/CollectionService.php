<?php

namespace Smorken\Tenancy\Services\Tenants;

use Smorken\Service\Contracts\Services\VO\CollectionResult;
use Smorken\Service\Services\BaseService;
use Smorken\Tenancy\Contracts\Storage\Tenant;

class CollectionService extends BaseService implements \Smorken\Tenancy\Contracts\Services\Tenants\CollectionService
{

    public function __construct(protected Tenant $provider, array $services = [])
    {
        parent::__construct($services);
    }

    public function active(): CollectionResult
    {
        $models = $this->getProvider()->active();
        return new \Smorken\Service\Services\VO\CollectionResult($models);
    }

    public function all(): CollectionResult
    {
        $models = $this->getProvider()->all();
        return new \Smorken\Service\Services\VO\CollectionResult($models);
    }

    public function getProvider(): Tenant
    {
        return $this->provider;
    }
}
