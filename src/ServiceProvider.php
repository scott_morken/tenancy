<?php

namespace Smorken\Tenancy;

use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\Routing\UrlGenerator;
use Smorken\Service\Invokables\Invoker;
use Smorken\Support\Contracts\Binder;
use Smorken\Tenancy\Bootstrappers\FilesystemBootstrapper;
use Smorken\Tenancy\Console\Commands\Installer;
use Smorken\Tenancy\Contracts\Config\Loader;
use Smorken\Tenancy\Features\ConfigureTenantViews;
use Smorken\Tenancy\Routes\AdminRoutes;
use Smorken\Tenancy\Routes\AssetRoutes;
use Stancl\Tenancy\Middleware\InitializeTenancyByDomain;

class ServiceProvider extends \Illuminate\Support\ServiceProvider
{

    public function boot(): void
    {
        $this->bootConfig();
        $this->bootViews();
        $this->loadMigrationsFrom(__DIR__.'/../database/migrations');
        $this->loadRoutes();
        $this->bindStorageProviders();
        $this->bindInvokables($this->app['config']->get('tenancyext.invokable_services', []));
        $this->loadCommands();
        $this->bootConfigureTenantViews();
        $this->rebindGlobalUrlAssetHelper();
    }

    public function register(): void
    {
        $this->bindPathsProvider();
        $this->bindConfigLoader();
    }

    protected function bindConfigLoader(): void
    {
        $this->app->bind(Loader::class, function (Application $app) {
            $overwriteKeys = $app['config']->get('tenancyext.overwrite_config_keys', []);
            return new \Smorken\Tenancy\Config\Loader(
                $app['config'],
                $app[\Smorken\Tenancy\Contracts\Paths::class],
                $overwriteKeys
            );
        });
    }

    protected function bindInvokables(array $invokables): void
    {
        $invoker = new Invoker($invokables, app_path());
        $invoker->handle($this->app);
    }

    protected function bindPathsProvider(): void
    {
        $this->app->bind(\Smorken\Tenancy\Contracts\Paths::class, function (Application $app) {
            $path = $app->basePath($app['config']->get('tenancyext.tenancy_base', 'tenants'));
            return new Paths($path);
        });
    }

    protected function bindStorageProviders(): void
    {
        /** @var Binder $binder */
        $binder = $this->app[Binder::class];
        $binder->bindAll($this->app['config']->get('tenancyext.providers', []));
    }

    protected function bootConfig(): void
    {
        $config = __DIR__.'/../config/tenancyext.php';
        $this->mergeConfigFrom($config, 'tenancyext');
        $this->publishes([$config => config_path('tenancyext.php')], 'config');
    }

    protected function bootConfigureTenantViews()
    {
        $use = $this->app['config']->get('tenancyext.tenants_use_central_views', false);
        ConfigureTenantViews::$removeOriginalViewPaths = $use;
    }

    protected function bootViews(): void
    {
        $this->loadViewsFrom(__DIR__.'/../views', 'tenancy');
        $this->publishes(
            [
                __DIR__.'/../views' => resource_path('/views/vendor/smorken/tenancy'),
            ],
            'views'
        );
    }

    protected function loadCommands(): void
    {
        $this->commands([
            Installer::class,
        ]);
    }

    protected function loadRoutes(): void
    {
        if ($this->app['config']->get('tenancyext.admin.load_routes', false)) {
            $prefix = implode('/', array_filter([
                $this->app['config']->get('tenancyext.admin.prefix', 'admin'),
                'tenant',
            ]));
            (new AdminRoutes())
                ->prefix($prefix)
                ->middleware($this->app['config']->get('tenancyext.admin.middleware',
                    ['web', 'auth', 'can:role-admin']))
                ->load($this->app);
        }
        if ($this->app['config']->get('tenancyext.assets.load_routes', false)) {
            (new AssetRoutes())
                ->middleware([
                    $this->app['config']->get('tenancyext.init_middleware', InitializeTenancyByDomain::class),
                ])
                ->load($this->app);
        }
    }

    protected function rebindGlobalUrlAssetHelper(): void
    {
        $this->app->extend('globalUrl', function (UrlGenerator $urlGenerator) {
            if ($this->app->bound(FilesystemBootstrapper::class)) {
                $instance = clone $urlGenerator;
                $instance->setAssetRoot($this->app[FilesystemBootstrapper::class]->originalPaths['asset_url']);
            } else {
                $instance = $urlGenerator;
            }

            return $instance;
        });
    }
}
