<?php

namespace Smorken\Tenancy\Storage\Eloquent;

use Illuminate\Support\Collection;
use Illuminate\Validation\Rule;
use Smorken\Tenancy\Concerns\HasApp;

class Tenant extends Base implements \Smorken\Tenancy\Contracts\Storage\Tenant
{

    use HasApp;

    public function active(): Collection
    {
        return $this->getCacheAssist()
                    ->remember(['active'], $this->getCacheAssist()->getCacheOptions()->defaultCacheTime, function () {
                        return $this->getModel()
                                    ->newQuery()
                                    ->defaultOrder()
                                    ->defaultWiths()
                                    ->activeIs()
                                    ->get();
                    });
    }

    public function current(): \Smorken\Tenancy\Contracts\Models\Tenant|null
    {
        return $this->getApp()[\Stancl\Tenancy\Contracts\Tenant::class];
    }

    public function syncDomains(
        \Smorken\Tenancy\Contracts\Models\Tenant $tenant,
        array $domains
    ): \Smorken\Tenancy\Contracts\Models\Tenant {
        $existing = $tenant->domains->pluck('domain')->toArray();
        $add = $this->getDomainsToAdd($existing, $domains);
        $delete = $this->getDomainsToDelete($existing, $domains);
        $tenant->domains()->whereIn('domain', $delete)->delete();
        $tenant->domains()->createMany(array_map(fn($v) => ['domain' => $v], $add));
        return $tenant;
    }

    public function validationRules(array $override = []): array
    {
        return [
            ...[
                'id' => [
                    'required',
                    'max:10',
                    Rule::unique($this->getModel()->getTable()),
                ],
                'name' => 'required|max:64',
                'active' => 'boolean',
            ],
            ...$override,
        ];
    }

    protected function getDomainsToAdd(array $existing, array $domains): array
    {
        $toAdd = [];
        foreach ($domains as $domain) {
            if ($domain && !in_array($domain, $existing, true)) {
                $toAdd[$domain] = $domain;
            }
        }
        return array_values($toAdd);
    }

    protected function getDomainsToDelete(array $existing, array $domains): array
    {
        $toDel = [];
        foreach ($existing as $existingDomain) {
            if ($existingDomain && !in_array($existingDomain, $domains, true)) {
                $toDel[$existingDomain] = $existingDomain;
            }
        }
        return array_values($toDel);
    }
}
