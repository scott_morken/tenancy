<?php

namespace Smorken\Tenancy\Providers\Services;

use Illuminate\Contracts\Foundation\Application;
use Smorken\Service\Invokables\Invokable;
use Smorken\Tenancy\Contracts\Paths;
use Smorken\Tenancy\Contracts\Services\Tenants\AssetService;
use Smorken\Tenancy\Contracts\Services\Tenants\CollectionService;
use Smorken\Tenancy\Contracts\Services\Tenants\RetrieveService;
use Smorken\Tenancy\Contracts\Storage\Tenant;

class TenantServices extends Invokable
{

    public function __invoke(): void
    {
        $this->getApp()->bind(AssetService::class, function (Application $app) {
            return new \Smorken\Tenancy\Services\Tenants\AssetService($app[Paths::class]);
        });
        $this->getApp()->bind(CollectionService::class, function (Application $app) {
            return new \Smorken\Tenancy\Services\Tenants\CollectionService($app[Tenant::class]);
        });
        $this->getApp()->bind(RetrieveService::class, function (Application $app) {
            return new \Smorken\Tenancy\Services\Tenants\RetrieveService($app[Tenant::class]);
        });
    }
}
