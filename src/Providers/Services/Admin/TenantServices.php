<?php

namespace Smorken\Tenancy\Providers\Services\Admin;

use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\Validation\Factory;
use Smorken\Service\Contracts\Services\FilterService;
use Smorken\Service\Contracts\Services\ValidationRulesService;
use Smorken\Service\Contracts\Services\ValidatorService;
use Smorken\Service\Invokables\Invokable;
use Smorken\Tenancy\Contracts\Services\Tenants\Admin\CrudServices;
use Smorken\Tenancy\Contracts\Services\Tenants\Admin\IndexService;
use Smorken\Tenancy\Contracts\Storage\Tenant;
use Smorken\Tenancy\Services\Tenants\Admin\CreateService;
use Smorken\Tenancy\Services\Tenants\Admin\UpdateService;

class TenantServices extends Invokable
{

    public function __invoke(): void
    {
        $this->getApp()->bind(CrudServices::class, function (Application $app) {
            $provider = $app[Tenant::class];
            $additionalProviders = [
                ValidatorService::class => new \Smorken\Service\Services\ValidatorService($app[Factory::class]),
                FilterService::class => new \Smorken\Tenancy\Services\Tenants\Admin\FilterService(),
                ValidationRulesService::class => new \Smorken\Tenancy\Services\Tenants\Admin\ValidationRulesService($provider),
            ];
            $cs = \Smorken\Tenancy\Services\Tenants\Admin\CrudServices::createByStorageProvider($provider,
                $additionalProviders);
            $cs->setCreateService(new CreateService($provider));
            $cs->setUpdateService(new UpdateService($provider));
            return $cs;
        });
        $this->getApp()->bind(IndexService::class, function (Application $app) {
            return new \Smorken\Tenancy\Services\Tenants\Admin\IndexService(
                $app[Tenant::class],
                [
                    FilterService::class => new \Smorken\Tenancy\Services\Tenants\Admin\FilterService(),
                ]
            );
        });
    }
}
