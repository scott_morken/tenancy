<?php

namespace Database\Factories\Smorken\Tenancy\Models\Eloquent;

use Illuminate\Database\Eloquent\Factories\Factory;
use Smorken\Tenancy\Models\Eloquent\Tenant;

class TenantFactory extends Factory
{

    protected $model = Tenant::class;

    public function definition(): array
    {
        return [
            'id' => 'foo',
            'data' => ['name' => 'Foo'],
        ];
    }
}
