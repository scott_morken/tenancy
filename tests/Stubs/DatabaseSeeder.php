<?php

namespace Tests\Smorken\Tenancy\Stubs;

use Database\Seeders\Smorken\Roles\Models\Eloquent\RoleSeeder;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{

    public function run(): void
    {
        $this->call(RoleSeeder::class);
    }
}
