<?php

namespace Tests\Smorken\Tenancy;

use Database\Seeders\Smorken\Roles\Models\Eloquent\RoleSeeder;
use Illuminate\Cache\CacheManager;
use Illuminate\Contracts\Cache\Repository;
use Orchestra\Testbench\TestCase;
use Smorken\CacheAssist\CacheAssist;
use Smorken\Tenancy\Contracts\Paths;
use Smorken\Tenancy\IdGenerators\SlugGenerator;
use Smorken\Tenancy\Models\Eloquent\Tenant;
use Smorken\Tenancy\ServiceProvider;
use Stancl\Tenancy\TenancyServiceProvider;
use Symfony\Component\Finder\Finder;

class TestBenchTestCase extends TestCase
{

    /**
     * Define database migrations.
     *
     * @return void
     */
    protected function defineDatabaseMigrations()
    {
        //$this->loadMigrationsFrom(__DIR__.'/database/migrations');
    }

    protected function defineEnvironment($app)
    {
        $app['config']->set('tenancyext.admin.layout', 'tenancy::master');
        $app['config']->set('tenancyext.admin.load_routes', true);
        $app['config']->set('tenancy.seeder_parameters.--class', RoleSeeder::class);
        $app['config']->set('tenancy.migration_parameters.--path', [
            __DIR__.'/database/migrations',
        ]);
        $app['config']->set('tenancy.tenant_model', Tenant::class);
        $app['config']->set('tenancy.id_generator', SlugGenerator::class);
        $app['config']->set('tenancy.bootstrappers', [
            \Stancl\Tenancy\Bootstrappers\DatabaseTenancyBootstrapper::class,
        ]);
        CacheAssist::setCache($app[CacheManager::class]);
    }

    protected function getPackageProviders($app)
    {
        return [
            \Smorken\Roles\ServiceProvider::class,
            \Smorken\Auth\Proxy\ServiceProvider::class,
            \Smorken\Support\ServiceProvider::class,
            TenancyServiceProvider::class,
            \Tests\Smorken\Tenancy\Stubs\TenancyServiceProvider::class,
            ServiceProvider::class,
        ];
    }

    protected function setUp(): void
    {
        parent::setUp();
        foreach (Finder::create()->files()->name('tenant*')->in(database_path()) as $file) {
            unlink($file);
        }
        $this->app[Paths::class] = new \Smorken\Tenancy\Paths(realpath(__DIR__.'/laravel/tenants'));
    }
}
