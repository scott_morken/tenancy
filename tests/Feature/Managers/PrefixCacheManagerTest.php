<?php

namespace Tests\Smorken\Tenancy\Feature\Managers;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Cache;
use Smorken\Tenancy\Bootstrappers\PrefixCacheBootstrapper;
use Smorken\Tenancy\Models\Eloquent\Tenant;
use Stancl\Tenancy\Bootstrappers\DatabaseTenancyBootstrapper;
use Tests\Smorken\Tenancy\TestBenchTestCase;

class PrefixCacheManagerTest extends TestBenchTestCase
{

    use RefreshDatabase;

    public function testAssetFromTenant(): void
    {
        $tenant = Tenant::factory()->create(['id' => 99]);
        $tenant->domains()->create(['domain' => 'localhost']);
        tenancy()->initialize($tenant);
        /** @var \Illuminate\Cache\CacheManager $cacheManager */
        $cacheManager = Cache::getFacadeRoot();
        $this->assertEquals('tenant_99_laravel_cache_', $cacheManager->getPrefix([]));
    }

    protected function defineEnvironment($app)
    {
        parent::defineEnvironment($app);
        $app['config']->set('tenancy.bootstrappers', [
            DatabaseTenancyBootstrapper::class,
            PrefixCacheBootstrapper::class,
        ]);
        $app['config']->set('cache.default', 'database');
    }
}
