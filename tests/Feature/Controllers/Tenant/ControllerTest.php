<?php

namespace Tests\Smorken\Tenancy\Feature\Controllers\Tenant;

use Database\Seeders\Smorken\Roles\Models\Eloquent\RoleSeeder;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Smorken\Auth\Proxy\Models\Eloquent\User;
use Smorken\Roles\Models\Eloquent\RoleUser;
use Smorken\Roles\ServiceProvider;
use Smorken\Tenancy\Models\Eloquent\Tenant;
use Stancl\Tenancy\TenantDatabaseManagers\SQLiteDatabaseManager;
use Tests\Smorken\Tenancy\TestBenchTestCase;

class ControllerTest extends TestBenchTestCase
{

    use RefreshDatabase;

    public function testCreate(): void
    {
        $user = User::factory(['id' => '1'])->create();
        $this->actingAs($user)
             ->get('/admin/tenant/create');
        $this->actingAs($user)
             ->post('/admin/tenant/create',
                 ['id' => 'foo', 'name' => 'Tenant 1'])
             ->assertRedirect('/admin/tenant');
        $this->actingAs($user)
             ->get('/admin/tenant')
             ->assertSee('Tenant 1')
             ->assertSee('--');
    }

    public function testCreateFailsValidation(): void
    {
        $user = User::factory(['id' => '1'])->create();
        $this->actingAs($user)
             ->get('/admin/tenant/create');
        $this->actingAs($user)
             ->post('/admin/tenant/create', [])
             ->assertRedirect('/admin/tenant/create')
             ->assertSessionHasErrors(['id', 'name']);
    }

    public function testCreateRequiresUniqueId(): void
    {
        $user = User::factory(['id' => '1'])->create();
        $tenant = Tenant::factory()->create(['id' => 't1', 'name' => 'T1']);
        $this->actingAs($user)
             ->get('/admin/tenant/create');
        $this->actingAs($user)
             ->post('/admin/tenant/create',
                 ['id' => 't1', 'name' => 'Tenant 1'])
             ->assertRedirect('/admin/tenant/create')
             ->assertSessionHasErrors(['id']);
    }

    public function testCreateWithCreateDatabase(): void
    {
        $user = User::factory(['id' => '1'])->create();
        $this->actingAs($user)
             ->get('/admin/tenant/create');
        $this->actingAs($user)
             ->post('/admin/tenant/create',
                 ['id' => 't1', 'name' => 'Tenant 1'])
             ->assertRedirect('/admin/tenant');
        $manager = $this->app[SQLiteDatabaseManager::class];
        $manager->setConnection('testing');
        $this->assertTrue($manager->databaseExists('tenantt1'));
    }

    public function testCreateWithDomain(): void
    {
        $user = User::factory(['id' => '1'])->create();
        $this->actingAs($user)
             ->get('/admin/tenant/create');
        $this->actingAs($user)
             ->post('/admin/tenant/create',
                 ['id' => 'foo', 'name' => 'Tenant 1', 'domains' => ['foo.example.edu']])
             ->assertRedirect('/admin/tenant');
        $this->actingAs($user)
             ->get('/admin/tenant')
             ->assertSee('Tenant 1')
             ->assertSee('foo.example.edu');
    }

    public function testDeleteNotFound(): void
    {
        $user = User::factory(['id' => '1'])->create();
        $this->actingAs($user)
             ->get('/admin/tenant/delete/99')
             ->assertStatus(404);
    }

    public function testDeletePostNotFound(): void
    {
        $user = User::factory(['id' => '1'])->create();
        $this->actingAs($user)
             ->post('/admin/tenant/delete/99')
             ->assertStatus(405);
    }

    public function testIndexNoRecords(): void
    {
        $user = User::factory(['id' => '1'])->create();
        $this->actingAs($user)
             ->get('/admin/tenant')
             ->assertSee('No records found');
    }

    public function testIndexWithRecords(): void
    {
        $user = User::factory(['id' => '1'])->create();
        $tenant = Tenant::factory()->create();
        $this->actingAs($user)
             ->get('/admin/tenant')
             ->assertSee($tenant->getTenantKey());
    }

    public function testNonAdminUserIsNotAuthorized(): void
    {
        $user = User::factory(['id' => '12345'])->create();
        $this->actingAs($user)
             ->get('/admin/tenant')
             ->assertStatus(403)
             ->assertSee('This action is unauthorized');
    }

    public function testUpdate(): void
    {
        $user = User::factory(['id' => '1'])->create();
        $tenant = Tenant::factory()->create();
        $this->actingAs($user)
             ->get('/admin/tenant/update/'.$tenant->id)
             ->assertSee($tenant->id)
             ->assertSee($tenant->name);
        $this->actingAs($user)
             ->post('/admin/tenant/update/'.$tenant->id,
                 ['id' => 't1', 'name' => 'Tenant 1'])
             ->assertRedirect('/admin/tenant');
        $this->actingAs($user)
             ->get('/admin/tenant')
             ->assertSee('Tenant 1');
    }

    public function testUpdateAddDomain(): void
    {
        $user = User::factory(['id' => '1'])->create();
        $tenant = Tenant::factory()->create();
        $tenant->domains()->create(['domain' => 'foo.example.edu']);
        $this->actingAs($user)
             ->get('/admin/tenant/update/'.$tenant->id)
             ->assertSee($tenant->id)
             ->assertSee($tenant->name);
        $this->actingAs($user)
             ->post('/admin/tenant/update/'.$tenant->id,
                 ['id' => 't1', 'name' => 'Tenant 1', 'domains' => ['foo.example.edu', 'bar.example.edu']])
             ->assertRedirect('/admin/tenant');
        $this->actingAs($user)
             ->get('/admin/tenant')
             ->assertSee('Tenant 1')
             ->assertSee('foo.example.edu;bar.example.edu');
    }

    public function testUpdateFailsValidation(): void
    {
        $user = User::factory(['id' => '1'])->create();
        $tenant = Tenant::factory()->create();
        $this->actingAs($user)
             ->get('/admin/tenant/update/'.$tenant->id);
        $this->actingAs($user)
             ->post('/admin/tenant/update/'.$tenant->id,
                 ['name' => ''])
             ->assertRedirect('/admin/tenant/update/'.$tenant->id)
             ->assertSessionHasErrors(['name']);
    }

    public function testUpdateNotFound(): void
    {
        $user = User::factory(['id' => '1'])->create();
        $this->actingAs($user)
             ->get('/admin/tenant/update/99')
             ->assertStatus(404);
    }

    public function testUpdateRemoveAndAddDomain(): void
    {
        $user = User::factory(['id' => '1'])->create();
        $tenant = Tenant::factory()->create();
        $tenant->domains()->create(['domain' => 'foo.example.edu']);
        $this->actingAs($user)
             ->get('/admin/tenant/update/'.$tenant->id)
             ->assertSee($tenant->id)
             ->assertSee($tenant->name);
        $this->actingAs($user)
             ->post('/admin/tenant/update/'.$tenant->id,
                 ['id' => 't1', 'name' => 'Tenant 1', 'domains' => ['bar.example.edu']])
             ->assertRedirect('/admin/tenant');
        $this->actingAs($user)
             ->get('/admin/tenant')
             ->assertSee('Tenant 1')
             ->assertDontSee('foo.example.edu')
             ->assertSee('bar.example.edu');
    }

    public function testView(): void
    {
        $user = User::factory(['id' => '1'])->create();
        $tenant = Tenant::factory()->create();
        $this->actingAs($user)
             ->get('/admin/tenant/view/'.$tenant->id)
             ->assertSee($tenant->getTenantKey());
    }

    public function testViewNotFound(): void
    {
        $user = User::factory(['id' => '1'])->create();
        $this->actingAs($user)
             ->get('/admin/tenant/view/99')
             ->assertStatus(404);
    }

    public function testViewWithDomain(): void
    {
        $user = User::factory(['id' => '1'])->create();
        $tenant = Tenant::factory()->create();
        $tenant->domains()->create(['domain' => 'foo.example.edu']);
        $this->actingAs($user)
             ->get('/admin/tenant/view/'.$tenant->id)
             ->assertSee($tenant->getTenantKey())
             ->assertSee('foo.example.edu');
    }

    protected function setUp(): void
    {
        parent::setUp();
        $this->seed(RoleSeeder::class);
        RoleUser::create(['user_id' => '1', 'role_id' => 1]);
        ServiceProvider::defineGates($this->app, true);
    }
}
