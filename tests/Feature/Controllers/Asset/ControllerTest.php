<?php

namespace Tests\Smorken\Tenancy\Feature\Controllers\Asset;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Cache;
use Smorken\Tenancy\Bootstrappers\FilesystemBootstrapper;
use Smorken\Tenancy\Bootstrappers\PrefixCacheBootstrapper;
use Smorken\Tenancy\Models\Eloquent\Tenant;
use Stancl\Tenancy\Bootstrappers\DatabaseTenancyBootstrapper;
use Tests\Smorken\Tenancy\TestBenchTestCase;

class ControllerTest extends TestBenchTestCase
{

    use RefreshDatabase;

    public function testAssetDoesNotAllowNonAsset(): void
    {
        $tenant = Tenant::factory()->create(['id' => 99]);
        $tenant->domains()->create(['domain' => 'localhost']);
        tenancy()->initialize($tenant);
        $response = $this->get('/tenancy/assets/../views/index.blade.php');
        $response->assertStatus(404);
    }

    public function testAssetFromCore(): void
    {
        $tenant = Tenant::factory()->create(['id' => 1]);
        $tenant->domains()->create(['domain' => 'localhost']);
        tenancy()->initialize($tenant);
        $response = $this->get('/tenancy/assets/images/logo.txt');
        $response->assertStatus(200);
        $this->assertEquals('core', trim($response->baseResponse->getFile()->getContent()));
    }

    public function testAssetFromTenant(): void
    {
        $tenant = Tenant::factory()->create(['id' => 99]);
        $tenant->domains()->create(['domain' => 'localhost']);
        tenancy()->initialize($tenant);
        $response = $this->get('/tenancy/assets/images/logo.txt');
        $response->assertStatus(200);
        $this->assertEquals('tenant_99', trim($response->baseResponse->getFile()->getContent()));
    }

    protected function defineEnvironment($app)
    {
        parent::defineEnvironment($app);
        $app['config']->set('tenancy.bootstrappers', [
            FilesystemBootstrapper::class,
            DatabaseTenancyBootstrapper::class,
        ]);
        $app['config']->set('tenancy.filesystem.asset_helper_tenancy', true);
        $app['config']->set('tenancy.filesystem.disks', []);
        $app['config']->set('tenancyext.assets.load_routes', true);
    }
}
