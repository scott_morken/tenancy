<?php

namespace Tests\Smorken\Tenancy\Unit\Managers;

use Mockery as m;
use PHPUnit\Framework\TestCase;
use Smorken\Tenancy\IdGenerators\SlugGenerator;
use Smorken\Tenancy\Managers\PrefixCacheManager;
use Stancl\Tenancy\Contracts\Tenant;
use Stancl\Tenancy\Contracts\UniqueIdentifierGenerator;
use Tests\Smorken\Tenancy\Concerns\HasApp;

class PrefixCacheManagerTest extends TestCase
{

    use HasApp;

    public function testCanPrefix(): void
    {
        $this->getApp()->bind(UniqueIdentifierGenerator::class, fn() => new SlugGenerator());
        $this->getApp()
             ->bind(Tenant::class, fn() => (new \Smorken\Tenancy\Models\Eloquent\Tenant())->forceFill(['id' => 'foo']));
        $sut = $this->getSut();
        $sut->extend('array', function ($app, $config) {
            return $this->getPrefix($config);
        });
        $store = $sut->store('custom');
        $this->assertEquals('tenant_foo', $store);
    }

    protected function getSut(): PrefixCacheManager
    {
        $this->getRepositoryWithConfig($this->getTenantConfig());
        return new PrefixCacheManager($this->getApp());
    }

    protected function getTenantConfig(): array
    {
        return [
            'cache' => [
                'stores' => [
                    'custom' => [
                        'driver' => 'array',
                    ],
                ],
            ],
            'tenancyext' => [
                'cache_prefix' => 'tenant',
            ],
            'tenancy' => [
                'bootstrappers' => [
                ],
                'features' => [
                ],
            ],
        ];
    }

    protected function tearDown(): void
    {
        parent::tearDown();
        m::close();
    }
}
