<?php

namespace Tests\Smorken\Tenancy\Unit\Features;

use PHPUnit\Framework\TestCase;
use Smorken\Tenancy\Features\ConfigureTenantViews;
use Smorken\Tenancy\Models\Eloquent\Tenant;
use Stancl\Tenancy\Events\TenancyEnded;
use Stancl\Tenancy\Events\TenancyInitialized;
use Stancl\Tenancy\Listeners\BootstrapTenancy;
use Stancl\Tenancy\Listeners\RevertToCentralContext;
use Tests\Smorken\Tenancy\Concerns\HasApp;
use Tests\Smorken\Tenancy\Concerns\HasTenancy;

class ConfigureTenantViewsTest extends TestCase
{

    use HasApp, HasTenancy;

    public function testEndAfterInitializeWithTenantPaths(): void
    {
        $sut = $this->getSut();
        $sut->bootstrap($this->getTenancy());
        $this->getEventDispatcher()->listen(TenancyInitialized::class, BootstrapTenancy::class);
        $this->getEventDispatcher()->listen(TenancyEnded::class, RevertToCentralContext::class);
        $tenant = $this->initTenant((new Tenant())->forceFill(['id' => 99]));
        $this->assertEquals([
            $this->getResourceDir('tenant_99/views'),
            $this->getResourceDir('core/views'),
            $this->getLaravelBaseDir('resources/views'),
            $this->getLaravelBaseDir('package/views'),
        ], $this->getViewFactory()->getFinder()->getPaths());
        //end
        $this->endTenancy($tenant);
        $this->assertEquals([
            $this->getLaravelBaseDir('resources/views'),
            $this->getLaravelBaseDir('package/views'),
        ], $this->getViewFactory()->getFinder()->getPaths());
    }

    public function testEndWithoutInitialize(): void
    {
        $sut = $this->getSut();
        $sut->bootstrap($this->getTenancy());
        $this->getEventDispatcher()->listen(TenancyInitialized::class, BootstrapTenancy::class);
        $this->getEventDispatcher()->listen(TenancyEnded::class, RevertToCentralContext::class);
        $this->endTenancy();
        $this->assertEquals([
            $this->getLaravelBaseDir('resources/views'),
            $this->getLaravelBaseDir('package/views'),
        ], $this->getViewFactory()->getFinder()->getPaths());
    }

    public function testInitialize(): void
    {
        $sut = $this->getSut();
        $sut->bootstrap($this->getTenancy());
        $this->getEventDispatcher()->listen(TenancyInitialized::class, BootstrapTenancy::class);
        $this->getEventDispatcher()->listen(TenancyEnded::class, RevertToCentralContext::class);
        $tenant = $this->initTenant();
        $this->assertEquals([
            $this->getResourceDir('core/views'),
            $this->getLaravelBaseDir('resources/views'),
            $this->getLaravelBaseDir('package/views'),
        ], $this->getViewFactory()->getFinder()->getPaths());
    }

    public function testInitializeWithRemovePaths(): void
    {
        $sut = $this->getSut();
        ConfigureTenantViews::$removeOriginalViewPaths = true;
        $sut->bootstrap($this->getTenancy());
        $this->getEventDispatcher()->listen(TenancyInitialized::class, BootstrapTenancy::class);
        $this->getEventDispatcher()->listen(TenancyEnded::class, RevertToCentralContext::class);
        $tenant = $this->initTenant();
        $this->assertEquals([
            $this->getResourceDir('core/views'),
        ], $this->getViewFactory()->getFinder()->getPaths());
    }

    public function testInitializeWithTenantPaths(): void
    {
        $sut = $this->getSut();
        $sut->bootstrap($this->getTenancy());
        $this->getEventDispatcher()->listen(TenancyInitialized::class, BootstrapTenancy::class);
        $this->getEventDispatcher()->listen(TenancyEnded::class, RevertToCentralContext::class);
        $tenant = $this->initTenant((new Tenant())->forceFill(['id' => 99]));
        $this->assertEquals([
            $this->getResourceDir('tenant_99/views'),
            $this->getResourceDir('core/views'),
            $this->getLaravelBaseDir('resources/views'),
            $this->getLaravelBaseDir('package/views'),
        ], $this->getViewFactory()->getFinder()->getPaths());
    }

    protected function getLaravelBaseDir(string $dir): string|false
    {
        return realpath($this->getPaths()->basePath('/../'.$dir));
    }

    protected function getResourceDir(string $dir): string
    {
        return $this->getPaths()->resourcePath($dir);
    }

    protected function getSut(): ConfigureTenantViews
    {
        $this->getRepository();
        $this->mergeConfigInto('view', $this->getViewConfig()['view']);
        $this->mergeConfigInto('tenancy', $this->getViewConfig()['tenancy']);
        ConfigureTenantViews::$removeOriginalViewPaths = false;
        return new ConfigureTenantViews(
            $this->getRepository(),
            $this->getViewFactory(),
            $this->getPaths()
        );
    }

    protected function getViewConfig(): array
    {
        return [
            'tenancy' => [
                'bootstrappers' => [

                ],
                'features' => [
                    ConfigureTenantViews::class,
                ],
            ],
            'view' => [
                'paths' => $this->getLaravelViewPaths(),
            ],
        ];
    }
}
