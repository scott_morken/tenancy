<?php

namespace Tests\Smorken\Tenancy\Unit\Features;

use Illuminate\Database\Connection;
use Illuminate\Database\Connectors\ConnectionFactory;
use Illuminate\Database\DatabaseManager;
use Illuminate\Support\Env;
use Mockery as m;
use PHPUnit\Framework\TestCase;
use Smorken\Tenancy\Features\ConfigureDatabaseConnections;
use Stancl\Tenancy\Events\TenancyEnded;
use Stancl\Tenancy\Events\TenancyInitialized;
use Stancl\Tenancy\Listeners\BootstrapTenancy;
use Stancl\Tenancy\Listeners\RevertToCentralContext;
use Tests\Smorken\Tenancy\Concerns\HasApp;
use Tests\Smorken\Tenancy\Concerns\HasTenancy;

class ConfigureDatabaseConnectionsTest extends TestCase
{

    use HasApp, HasTenancy;

    public function testInitializeAndEnd(): void
    {
        $sut = $this->getSut();
        $sut->bootstrap($this->getTenancy());
        $this->getEventDispatcher()->listen(TenancyInitialized::class, BootstrapTenancy::class);
        $this->getEventDispatcher()->listen(TenancyEnded::class, RevertToCentralContext::class);
        Env::getRepository()->set('SIS_1_USERNAME', 'foo');
        Env::getRepository()->set('SIS_1_PASSWORD', 'bar');
        $this->getDatabaseManager()->shouldReceive('extend')
             ->once()
             ->andReturnUsing(function ($connectionName, $callable) {
                 $this->assertEquals('sis', $connectionName);
                 return $callable($this->getApp()['config']->get('database.connections.sis'), 'sis');
             });
        $expectedConfig = [
            'driver' => 'mysql',
            'host' => '127.0.0.1',
            'port' => '3306',
            'database' => 'sis_db',
            'username' => 'foo',
            'password' => 'bar',
            'charset' => 'utf8mb4',
            'collation' => 'utf8mb4_unicode_ci',
            'prefix' => '',
            'prefix_indexes' => true,
            'strict' => true,
            'engine' => null,
            'options' => [],
        ];
        $this->getConnectionFactory()->shouldReceive('make')
             ->once()
             ->andReturnUsing(function (array $config, string $name) use ($expectedConfig) {
                 $this->assertEquals($expectedConfig, $config);
                 $this->assertEquals('sis', $name);
                 return m::mock(Connection::class);
             });
        $this->getDatabaseManager()->shouldReceive('purge')
             ->once()
             ->with('sis');
        $tenant = $this->initTenant();
        $this->assertEquals($expectedConfig, $this->getApp()['config']->get('database.connections.sis'));
        //end
        $expectedConfig = [
            'driver' => 'mysql',
            'host' => '127.0.0.1',
            'port' => '3306',
            'database' => 'sis_db',
            'username' => '',
            'password' => '',
            'charset' => 'utf8mb4',
            'collation' => 'utf8mb4_unicode_ci',
            'prefix' => '',
            'prefix_indexes' => true,
            'strict' => true,
            'engine' => null,
            'options' => [],
        ];
        $this->getDatabaseManager()->shouldReceive('extend')
             ->once()
             ->andReturnUsing(function ($connectionName, $callable) {
                 $this->assertEquals('sis', $connectionName);
                 return $callable($this->getApp()['config']->get('database.connections.sis'), 'sis');
             });
        $this->getConnectionFactory()->shouldReceive('make')
             ->once()
             ->andReturnUsing(function (array $config, string $name) use ($expectedConfig) {
                 $this->assertEquals($expectedConfig, $config);
                 $this->assertEquals('sis', $name);
                 return m::mock(Connection::class);
             });
        $this->getDatabaseManager()->shouldReceive('purge')
             ->once()
             ->with('sis');
        $this->endTenancy();
        $this->assertEquals($expectedConfig, $this->getApp()['config']->get('database.connections.sis'));
    }

    public function testEndWithoutInitialize(): void
    {
        $sut = $this->getSut();
        $sut->bootstrap($this->getTenancy());
        $this->getEventDispatcher()->listen(TenancyInitialized::class, BootstrapTenancy::class);
        $this->getEventDispatcher()->listen(TenancyEnded::class, RevertToCentralContext::class);
        $expectedConfig = [
            'driver' => 'mysql',
            'host' => '127.0.0.1',
            'port' => '3306',
            'database' => 'sis_db',
            'username' => '',
            'password' => '',
            'charset' => 'utf8mb4',
            'collation' => 'utf8mb4_unicode_ci',
            'prefix' => '',
            'prefix_indexes' => true,
            'strict' => true,
            'engine' => null,
            'options' => [],
        ];
        $this->getDatabaseManager()->shouldReceive('extend')
             ->once()
             ->andReturnUsing(function ($connectionName, $callable) {
                 $this->assertEquals('sis', $connectionName);
                 return $callable($this->getApp()['config']->get('database.connections.sis'), 'sis');
             });
        $this->getConnectionFactory()->shouldReceive('make')
             ->once()
             ->andReturnUsing(function (array $config, string $name) use ($expectedConfig) {
                 $this->assertEquals($expectedConfig, $config);
                 $this->assertEquals('sis', $name);
                 return m::mock(Connection::class);
             });
        $this->getDatabaseManager()->shouldReceive('purge')
             ->once()
             ->with('sis');
        $this->endTenancy();
        $this->assertEquals($expectedConfig, $this->getApp()['config']->get('database.connections.sis'));
    }

    public function testInitializeWithUsernameAndPassword(): void
    {
        $sut = $this->getSut();
        $sut->bootstrap($this->getTenancy());
        $this->getEventDispatcher()->listen(TenancyInitialized::class, BootstrapTenancy::class);
        $this->getEventDispatcher()->listen(TenancyEnded::class, RevertToCentralContext::class);
        Env::getRepository()->set('SIS_1_USERNAME', 'foo');
        Env::getRepository()->set('SIS_1_PASSWORD', 'bar');
        $this->getDatabaseManager()->shouldReceive('extend')
             ->once()
             ->andReturnUsing(function ($connectionName, $callable) {
                 $this->assertEquals('sis', $connectionName);
                 return $callable($this->getApp()['config']->get('database.connections.sis'), 'sis');
             });
        $expectedConfig = [
            'driver' => 'mysql',
            'host' => '127.0.0.1',
            'port' => '3306',
            'database' => 'sis_db',
            'username' => 'foo',
            'password' => 'bar',
            'charset' => 'utf8mb4',
            'collation' => 'utf8mb4_unicode_ci',
            'prefix' => '',
            'prefix_indexes' => true,
            'strict' => true,
            'engine' => null,
            'options' => [],
        ];
        $this->getConnectionFactory()->shouldReceive('make')
             ->once()
             ->andReturnUsing(function (array $config, string $name) use ($expectedConfig) {
                 $this->assertEquals($expectedConfig, $config);
                 $this->assertEquals('sis', $name);
                 return m::mock(Connection::class);
             });
        $this->getDatabaseManager()->shouldReceive('purge')
             ->once()
             ->with('sis');
        $tenant = $this->initTenant();
        $this->assertEquals($expectedConfig, $this->getApp()['config']->get('database.connections.sis'));
    }

    public function testInitializeWithoutUsernameOrPassword(): void
    {
        $sut = $this->getSut();
        Env::getRepository()->clear('SIS_1_USERNAME');
        Env::getRepository()->clear('SIS_1_PASSWORD');
        $sut->bootstrap($this->getTenancy());
        $this->getEventDispatcher()->listen(TenancyInitialized::class, BootstrapTenancy::class);
        $this->getEventDispatcher()->listen(TenancyEnded::class, RevertToCentralContext::class);
        $this->getDatabaseManager()->shouldReceive('extend')
             ->once()
             ->andReturnUsing(function ($connectionName, $callable) {
                 $this->assertEquals('sis', $connectionName);
                 return $callable($this->getApp()['config']->get('database.connections.sis'), 'sis');
             });
        $expectedConfig = [
            'driver' => 'mysql',
            'host' => '127.0.0.1',
            'port' => '3306',
            'database' => 'sis_db',
            'username' => '',
            'password' => '',
            'charset' => 'utf8mb4',
            'collation' => 'utf8mb4_unicode_ci',
            'prefix' => '',
            'prefix_indexes' => true,
            'strict' => true,
            'engine' => null,
            'options' => [],
        ];
        $this->getConnectionFactory()->shouldReceive('make')
             ->once()
             ->andReturnUsing(function (array $config, string $name) use ($expectedConfig) {
                 $this->assertEquals($expectedConfig, $config);
                 $this->assertEquals('sis', $name);
                 return m::mock(Connection::class);
             });
        $this->getDatabaseManager()->shouldReceive('purge')
             ->once()
             ->with('sis');
        $tenant = $this->initTenant();
        $this->assertEquals($expectedConfig, $this->getApp()['config']->get('database.connections.sis'));
    }

    protected function getConnectionFactory(): ConnectionFactory|m\MockInterface
    {
        if (!$this->getApp()->has('db.factory')) {
            $f = m::mock(ConnectionFactory::class);
            $this->getApp()->bind('db.factory', fn() => $f);
        }
        return $this->getApp()['db.factory'];
    }

    protected function getDatabaseConfig(): array
    {
        return [
            'tenancy' => [
                'bootstrappers' => [

                ],
                'features' => [
                    ConfigureDatabaseConnections::class,
                ],
            ],
            'tenancyext' => [
                'connections' => [
                    'sis' => null,
                ],
            ],
            'database' => [
                'default' => 'central',

                'connections' => [

                    'sis' => [
                        'driver' => 'mysql',
                        'host' => '127.0.0.1',
                        'port' => '3306',
                        'database' => 'sis_db',
                        'username' => '',
                        'password' => '',
                        'charset' => 'utf8mb4',
                        'collation' => 'utf8mb4_unicode_ci',
                        'prefix' => '',
                        'prefix_indexes' => true,
                        'strict' => true,
                        'engine' => null,
                        'options' => [],
                    ],

                ],
            ],
        ];
    }

    protected function getDatabaseManager(): DatabaseManager|m\MockInterface
    {
        if (!$this->getApp()->has('db')) {
            $m = m::mock(DatabaseManager::class);
            $this->getApp()->bind('db', fn() => $m);
        }
        return $this->getApp()['db'];
    }

    protected function getSut(array $connectionConfigs = []): ConfigureDatabaseConnections
    {
        if ($connectionConfigs) {
            $this->mergeConfigInto('tenancyext.connections', $connectionConfigs);
        }
        $this->getDatabaseManager();
        $this->getConnectionFactory();
        $this->getRepository();
        $this->mergeConfigInto('tenancyext', $this->getDatabaseConfig()['tenancyext']);
        $this->mergeConfigInto('tenancy', $this->getDatabaseConfig()['tenancy']);
        $this->mergeConfigInto('database', $this->getDatabaseConfig()['database']);
        return new ConfigureDatabaseConnections($this->getApp());
    }

    protected function tearDown(): void
    {
        parent::tearDown();
        m::close();
    }
}
