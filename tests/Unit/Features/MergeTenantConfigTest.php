<?php

namespace Tests\Smorken\Tenancy\Unit\Features;

use Illuminate\Container\Container;
use Illuminate\Support\Facades\Event;
use PHPUnit\Framework\TestCase;
use Smorken\Tenancy\Features\MergeTenantConfig;
use Stancl\Tenancy\Events\TenancyEnded;
use Stancl\Tenancy\Events\TenancyInitialized;
use Stancl\Tenancy\Listeners\BootstrapTenancy;
use Stancl\Tenancy\Listeners\RevertToCentralContext;
use Tests\Smorken\Tenancy\Concerns\HasTenancy;

class MergeTenantConfigTest extends TestCase
{

    use HasTenancy;

    protected array $originalConfig = [];

    public function testEndWithoutInitialize(): void
    {
        $sut = $this->getSut();
        $sut->bootstrap($this->getTenancy());
        $this->getEventDispatcher()->listen(TenancyInitialized::class, BootstrapTenancy::class);
        $this->getEventDispatcher()->listen(TenancyEnded::class, RevertToCentralContext::class);
        $this->endTenancy();
        $this->assertEquals($this->originalConfig, $this->getApp()['config']->all());
    }

    public function testInitializeAndEnd(): void
    {
        $sut = $this->getSut();
        $this->assertSame($this->getApp()->get('events'), app('events'));
        $sut->bootstrap($this->getTenancy());
        $this->getEventDispatcher()->listen(TenancyInitialized::class, BootstrapTenancy::class);
        $this->getEventDispatcher()->listen(TenancyEnded::class, RevertToCentralContext::class);
        $tenant = $this->initTenant();
        $this->assertEquals($this->getExpectedTenant(), $this->getApp()['config']->all());
        //end
        $this->endTenancy($tenant);
        $this->assertEquals($this->originalConfig, $this->getApp()['config']->all());
    }

    public function testInitializeChangesToTenantConfig(): void
    {
        $sut = $this->getSut();
        $sut->bootstrap($this->getTenancy());
        $this->getEventDispatcher()->listen(TenancyInitialized::class, BootstrapTenancy::class);
        $this->getEventDispatcher()->listen(TenancyEnded::class, RevertToCentralContext::class);
        $tenant = $this->initTenant();
        $this->assertEquals($this->getExpectedTenant(), $this->getApp()['config']->all());
    }

    protected function getExpectedTenant(): array
    {
        return [
            'app' => [
                'name' => 'Landlord',
            ],
            'menus' => [
                'role-admin' => [
                    [
                        'name' => 'Admin Work',
                    ],
                ],
            ],
            'filesystems' => [
                'disks' => [
                    'local' => [
                        'driver' => 'foo',
                        'root' => '/',
                        'throw' => false,
                    ],
                    'public' => [
                        'driver' => 'local',
                        'root' => '/app/public',
                        'url' => 'https://landlord.example.edu',
                        'visibility' => 'public',
                        'throw' => false,
                    ],
                ],
            ],
            'tenancy' => [
                'bootstrappers' => [

                ],
                'features' => [
                    MergeTenantConfig::class,
                ],
                'tenant_model' => 'Smorken\Tenancy\Models\Eloquent\Tenant',
                'id_generator' => 'Smorken\Tenancy\IdGenerators\SlugGenerator',
            ],
            'database' => [
                'default' => 'test',
            ],
        ];
    }

    protected function getSut(): MergeTenantConfig
    {
        $this->getRepository();
        $this->mergeConfigInto('tenancy', $this->getTenantConfig());
        $this->originalConfig = $this->getApp()['config']->all();
        return new MergeTenantConfig(new \Smorken\Tenancy\Config\Loader(
            $this->getApp()['config'],
            $this->getPaths(),
            ['menus']
        ));
    }

    protected function getTenantConfig(): array
    {
        return [
            'bootstrappers' => [

            ],
            'features' => [
                MergeTenantConfig::class,
            ],
        ];
    }
}
