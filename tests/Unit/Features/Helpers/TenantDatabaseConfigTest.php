<?php

namespace Tests\Smorken\Tenancy\Unit\Features\Helpers;

use Illuminate\Support\Env;
use PHPUnit\Framework\TestCase;
use Smorken\Tenancy\Features\Helpers\TenantDatabaseConfig;
use Smorken\Tenancy\Models\Eloquent\Tenant;

class TenantDatabaseConfigTest extends TestCase
{

    public function testBaseConfigValues(): void
    {
        $sut = new TenantDatabaseConfig();
        $tenant = (new Tenant())->forceFill([
            'id' => 1, 'name' => 'Tenant 1',
        ]);
        $config = $sut->create($tenant);
        $this->assertEquals(['username' => null, 'password' => null, 'host' => null, 'database' => null], $config);
    }

    public function testGetEnvKeysFromConfigMap(): void
    {
        $sut = new TenantDatabaseConfig();
        $tenant = (new Tenant())->forceFill([
            'id' => 1, 'name' => 'Tenant 1',
        ]);
        $keys = [];
        foreach ($sut->getConfigMap() as $envSuffix => $configKey) {
            $keys[] = $sut->getEnvKey($tenant, $envSuffix);
        }
        $this->assertEquals(['TENANT_1_HOST', 'TENANT_1_DATABASE', 'TENANT_1_USERNAME', 'TENANT_1_PASSWORD'], $keys);
    }

    public function testOverrideConnectionAndConfigMap(): void
    {
        Env::getRepository()->set('FOO_1_USERNAME', 'foo');
        Env::getRepository()->set('FOO_1_PASSWORD', 'bar');
        Env::getRepository()->set('FOO_1_HOST', 'foo.example.edu');
        $sut = new TenantDatabaseConfig('foo', ['username' => 'username', 'password' => 'password', 'host' => 'host']);
        $tenant = (new Tenant())->forceFill([
            'id' => 1, 'name' => 'Tenant 1',
        ]);
        $expected = [
            'username' => 'foo',
            'password' => 'bar',
            'host' => 'foo.example.edu',
        ];
        $this->assertEquals($expected, $sut->create($tenant));
    }
}
