<?php

namespace Tests\Smorken\Tenancy\Unit;

use PHPUnit\Framework\TestCase;
use Smorken\Tenancy\Contracts\Paths;

class PathsTest extends TestCase
{

    public function testBasePath(): void
    {
        $sut = $this->getSut();
        $this->assertEquals(__DIR__, $sut->basePath());
    }

    public function testBasePathWithPath(): void
    {
        $sut = $this->getSut();
        $this->assertEquals(__DIR__.'/foo', $sut->basePath('foo'));
    }

    public function testConfigPath(): void
    {
        $sut = $this->getSut();
        $this->assertEquals(__DIR__.'/config', $sut->configPath());
    }

    public function testConfigPathWithPath(): void
    {
        $sut = $this->getSut();
        $this->assertEquals(__DIR__.'/config/foo', $sut->configPath('foo'));
    }

    public function testResourcePath(): void
    {
        $sut = $this->getSut();
        $this->assertEquals(__DIR__.'/resources', $sut->resourcePath());
    }

    public function testResourcePathWithPath(): void
    {
        $sut = $this->getSut();
        $this->assertEquals(__DIR__.'/resources/foo', $sut->resourcePath('foo'));
    }

    protected function getSut(): Paths
    {
        return new \Smorken\Tenancy\Paths(__DIR__);
    }
}
