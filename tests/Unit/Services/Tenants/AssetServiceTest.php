<?php

namespace Tests\Smorken\Tenancy\Unit\Services\Tenants;

use PHPUnit\Framework\TestCase;
use Smorken\Tenancy\Contracts\Services\Tenants\AssetService;
use Smorken\Tenancy\Models\Eloquent\Tenant;
use Tests\Smorken\Tenancy\Concerns\HasApp;

class AssetServiceTest extends TestCase
{

    use HasApp;

    public function testCoreItemFailsForNonAsset(): void
    {
        $sut = $this->getSut();
        $tenant = (new Tenant())->forceFill(['id' => 1]);
        $this->assertFalse($sut->path($tenant, '/../views/index.blade.php'));
    }

    public function testCoreItemFound(): void
    {
        $sut = $this->getSut();
        $tenant = (new Tenant())->forceFill(['id' => 1]);
        $this->assertEquals($this->getPaths()->resourcePath('core/assets/images/logo.png'),
            $sut->path($tenant, '/images/logo.png'));
    }

    public function testEnsureSingleSlash(): void
    {
        $sut = $this->getSut();
        $tenant = (new Tenant())->forceFill(['id' => 1]);
        $this->assertEquals($this->getPaths()->resourcePath('core/assets/images/logo.png'),
            $sut->path($tenant, '//images/logo.png'));
    }

    public function testNoItemFound(): void
    {
        $sut = $this->getSut();
        $tenant = (new Tenant())->forceFill(['id' => 99]);
        $this->assertFalse($sut->path($tenant, '/images/myphoto.png'));
    }

    public function testTenantItemFound(): void
    {
        $sut = $this->getSut();
        $tenant = (new Tenant())->forceFill(['id' => 99]);
        $this->assertEquals($this->getPaths()->resourcePath('tenant_99/assets/images/logo.png'),
            $sut->path($tenant, '/images/logo.png'));
    }

    protected function getSut(): AssetService
    {
        return new \Smorken\Tenancy\Services\Tenants\AssetService($this->getPaths());
    }
}
