<?php

namespace Tests\Smorken\Tenancy\Unit\IdGenerators;

use PHPUnit\Framework\TestCase;
use Smorken\Tenancy\IdGenerators\SlugGenerator;
use Smorken\Tenancy\Models\Eloquent\Tenant;
use Tests\Smorken\Tenancy\Concerns\HasApp;

class SlugGeneratorTest extends TestCase
{

    // Needed to bind UniqueIdentifierGenerator
    use HasApp;

    public function testCreatesSlug(): void
    {
        $tenant = (new Tenant())->forceFill(['id' => 'Foo Bar Biz']);
        $this->assertEquals('foo_bar_biz', SlugGenerator::generate($tenant));
    }
}
