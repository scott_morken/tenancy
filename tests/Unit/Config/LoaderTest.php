<?php

namespace Tests\Smorken\Tenancy\Unit\Config;

use Illuminate\Config\Repository;
use PHPUnit\Framework\TestCase;
use Smorken\Tenancy\Contracts\Config\Loader;
use Smorken\Tenancy\Models\Eloquent\Tenant;
use Tests\Smorken\Tenancy\Concerns\HasApp;

class LoaderTest extends TestCase
{

    use HasApp;

    public function testTenantCanOverwriteCore(): void
    {
        $sut = $this->getSut();
        $tenant = (new Tenant())->forceFill([
            'id' => 1, 'name' => 'Tenant 1', 'domain' => 't1.example.edu', 'database' => 't_1',
        ]);
        $sut->bootstrap($tenant);
        $expected = [
            'app' => [
                'name' => 'Landlord',
            ],
            'menus' => [
                'role-admin' => [
                    [
                        'name' => 'Admin Work',
                    ],
                ],
            ],
            'filesystems' => [
                'disks' => [
                    'local' => [
                        'driver' => 'foo',
                        'root' => '/',
                        'throw' => false,
                    ],
                    'public' => [
                        'driver' => 'local',
                        'root' => '/app/public',
                        'url' => 'https://landlord.example.edu',
                        'visibility' => 'public',
                        'throw' => false,
                    ],
                ],
            ],
        ];
        $this->assertEquals($expected, $sut->getConfig()->all());
    }

    public function testTenantCanOverwriteTenantSpecific(): void
    {
        $sut = $this->getSut();
        $tenant = (new Tenant())->forceFill([
            'id' => 99, 'name' => 'Tenant 99', 'domain' => 'tenant99.example.edu', 'database' => 't_99',
        ]);
        $sut->bootstrap($tenant);
        $expected = [
            'app' => [
                'name' => 'Foo Bar',
            ],
            'menus' => [
                'role-admin' => [
                    [
                        'name' => 'Admin Work',
                    ],
                ],
            ],
            'filesystems' => [
                'disks' => [
                    'local' => [
                        'driver' => 'foo',
                        'root' => '/',
                        'throw' => false,
                    ],
                    'public' => [
                        'driver' => 'local',
                        'root' => '/app/public',
                        'url' => 'https://tenant99.example.edu',
                        'visibility' => 'public',
                        'throw' => false,
                    ],
                ],
            ],
            'other' => [
                'foo' => 'bar',
            ],
        ];
        $this->assertEquals($expected, $sut->getConfig()->all());
    }

    protected function getConfig(): array
    {
        return [
            'app' => [
                'name' => 'Landlord',
            ],
            'menus' => [
                'guest' => [
                    [
                        'name' => 'L Menu',
                    ],
                ],
            ],
            'filesystems' => [
                'disks' => [
                    'local' => [
                        'driver' => 'local',
                        'root' => '/',
                        'throw' => false,
                    ],
                    'public' => [
                        'driver' => 'local',
                        'root' => '/app/public',
                        'url' => 'https://landlord.example.edu',
                        'visibility' => 'public',
                        'throw' => false,
                    ],
                ],
            ],
        ];
    }

    protected function getSut(): Loader
    {
        return new \Smorken\Tenancy\Config\Loader(
            new Repository($this->getConfig()),
            $this->getPaths(),
            ['menus']
        );
    }
}
