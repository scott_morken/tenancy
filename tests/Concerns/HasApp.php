<?php

namespace Tests\Smorken\Tenancy\Concerns;

use Illuminate\Config\Repository;
use Illuminate\Container\Container;
use Illuminate\Contracts\Events\Dispatcher;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Contracts\View\Factory;
use Illuminate\Foundation\Bootstrap\LoadConfiguration;
use Illuminate\Support\Facades\Facade;
use Illuminate\View\Engines\EngineResolver;
use Illuminate\View\FileViewFinder;
use Smorken\Tenancy\Contracts\Config\Loader;
use Smorken\Tenancy\Contracts\Paths;
use Smorken\Tenancy\IdGenerators\SlugGenerator;
use Stancl\Tenancy\Contracts\UniqueIdentifierGenerator;
use Stancl\Tenancy\Tenancy;

trait HasApp
{

    use HasLaravelPath;

    protected ?\Illuminate\Contracts\Container\Container $app = null;

    /**
     * getLoader and getRepository/WithConfig should be loaded
     * separately to avoid overwriting the config
     *
     * @var array|string[]
     */
    protected array $initMethods = [
        'getApp',
        'getEventDispatcher',
        'getTenancy',
        'getPaths',
        'getIdGenerator',
        'getViewFactory',
    ];

    protected bool $initialized = false;

    protected function getApp(): \Illuminate\Contracts\Container\Container
    {
        if (!$this->app) {
            $app = new \Illuminate\Foundation\Application(realpath(__DIR__.'/../laravel'));
            Container::setInstance($app);
            $this->app = Container::getInstance();
            Facade::clearResolvedInstances();
            Facade::setFacadeApplication($this->app);
        }
        return $this->app;
    }

    protected function getBaseConfig(array $merge = []): array
    {
        return array_replace_recursive($merge, [
            'app' => [
                'name' => 'Landlord',
            ],
            'menus' => [
                'guest' => [
                    [
                        'name' => 'L Menu',
                    ],
                ],
            ],
            'filesystems' => [
                'disks' => [
                    'local' => [
                        'driver' => 'local',
                        'root' => '/',
                        'throw' => false,
                    ],
                    'public' => [
                        'driver' => 'local',
                        'root' => '/app/public',
                        'url' => 'https://landlord.example.edu',
                        'visibility' => 'public',
                        'throw' => false,
                    ],
                ],
            ],
        ]);
    }

    protected function getEventDispatcher(): Dispatcher
    {
        if (!$this->getApp()->has('events')) {
            $dispatcher = new \Illuminate\Events\Dispatcher($this->getApp());
            $this->getApp()->bind('events', fn() => $dispatcher);
        }
        return $this->getApp()->get('events');
    }

    protected function getIdGenerator(): UniqueIdentifierGenerator
    {
        if (!$this->getApp()->has(UniqueIdentifierGenerator::class)) {
            $this->getApp()->bind(UniqueIdentifierGenerator::class, SlugGenerator::class);
        }
        return $this->getApp()->get(UniqueIdentifierGenerator::class);
    }

    protected function getLoader(): Loader
    {
        if (!$this->getApp()->has(Loader::class)) {
            $this->getApp()->bind(Loader::class, fn() => new \Smorken\Tenancy\Config\Loader($this->getRepository(),
                $this->getPaths(), ['menus']));
        }
        return $this->getApp()->get(Loader::class);
    }

    protected function getPaths(): Paths
    {
        if (!$this->getApp()->has(Paths::class)) {
            $this->getApp()
                 ->bind(Paths::class, fn() => new \Smorken\Tenancy\Paths(realpath(__DIR__.'/../laravel/tenants')));
        }
        return $this->getApp()->get(Paths::class);
    }

    protected function getRepository(): Repository
    {
        if (!$this->getApp()->has('config')) {
            (new LoadConfiguration())->bootstrap($this->getApp());
//            Config::setFacadeApplication($this->getApp());
        }
        return $this->getApp()->get('config');
    }

    protected function getRepositoryWithConfig(array $config = [], bool $rebind = false): Repository
    {
        if (!$this->getApp()->has('config') || $rebind) {
            $repository = new Repository($config ?: $this->getBaseConfig());
            $this->getApp()->bind('config', fn() => $repository);
//            Container::getInstance()->bind('config', fn() => $repository);
//            Config::setFacadeApplication($this->getApp());
        }
        return $this->getApp()->get('config');
    }

    protected function getTenancy(): Tenancy
    {
        if (!$this->getApp()->has(Tenancy::class)) {
            $this->getApp()->bind(Tenancy::class, fn() => new Tenancy());
        }
        return $this->getApp()->get(Tenancy::class);
    }

    protected function getViewFactory(): Factory
    {
        if (!$this->getApp()->has('view')) {
            $files = $this->getFiles();
            $viewFactory = new \Illuminate\View\Factory(new EngineResolver(),
                new FileViewFinder($files, $this->getLaravelViewPaths()), $this->getEventDispatcher());
            $this->getApp()->bind('view', fn() => $viewFactory);
        }
        return $this->getApp()->get('view');
    }

    protected function getFiles(): Filesystem
    {
        if (!$this->getApp()->has('files')) {
            $this->getApp()->bind('files', fn() => new Filesystem());
        }
        return $this->getApp()->get('files');
    }

    protected function initializeApp(): void
    {
        if (!$this->initialized) {
            foreach ($this->initMethods as $method) {
                $this->$method();
            }
            $this->initialized = true;
        }
    }

    protected function mergeConfigInto(string $key, array $items): void
    {
        foreach ($items as $k => $v) {
            $this->getApp()->get('config')->set("$key.$k", $v);
        }
    }

    protected function resetApp(): void
    {
        $this->getApp()->flush();
        $this->app = null;
    }

    protected function setUp(): void
    {
        parent::setUp();
        $this->initializeApp();
    }

    protected function tearDown(): void
    {
        parent::tearDown();
        $this->resetApp();
    }
}
