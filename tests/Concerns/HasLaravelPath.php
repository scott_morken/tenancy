<?php

namespace Tests\Smorken\Tenancy\Concerns;

trait HasLaravelPath
{

    protected function getLaravelViewPaths(): array
    {
        return [
            realpath(__DIR__.'/../laravel/resources/views'),
            realpath(__DIR__.'/../laravel/package/views'),
        ];
    }
}
