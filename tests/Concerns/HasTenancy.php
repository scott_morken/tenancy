<?php

namespace Tests\Smorken\Tenancy\Concerns;

use Smorken\Tenancy\Models\Eloquent\Tenant;

trait HasTenancy
{

    use HasApp;

    protected function endTenancy(
        ?\Smorken\Tenancy\Contracts\Models\Tenant $tenant = null,
        bool $initialized = true
    ): void {
        $tenancy = $this->getTenancy();
        if (!$tenancy->initialized && $initialized) {
            $tenancy->initialized = $initialized;
            if (!$tenant) {
                $tenant = (new Tenant())->forceFill(['id' => '1', 'data' => ['name' => 'Tenant 1']]);
            }
            $tenancy->tenant = $tenant;
        }

        $tenancy->end();
    }

    protected function initTenant(?\Smorken\Tenancy\Contracts\Models\Tenant $tenant = null
    ): \Smorken\Tenancy\Contracts\Models\Tenant {
        if (!$tenant) {
            $tenant = (new Tenant())->forceFill(['id' => '1', 'data' => ['name' => 'Tenant 1']]);
        }
        $this->getTenancy()->initialize($tenant);
        return $tenant;
    }
}
