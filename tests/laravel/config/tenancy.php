<?php
return [
    'tenant_model' => \Smorken\Tenancy\Models\Eloquent\Tenant::class,
    'id_generator' => \Smorken\Tenancy\IdGenerators\SlugGenerator::class,
];
