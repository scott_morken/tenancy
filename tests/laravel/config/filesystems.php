<?php
return [
    'disks' => [
        'local' => [
            'driver' => 'local',
            'root' => '/',
            'throw' => false,
        ],
        'public' => [
            'driver' => 'local',
            'root' => '/app/public',
            'url' => 'https://landlord.example.edu',
            'visibility' => 'public',
            'throw' => false,
        ],
    ],
];
