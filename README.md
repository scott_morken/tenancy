## Tenancy helper

Uses [stancl/tenancy](https://tenancyforlaravel.com/)

Install tenancy config and tenant migrations directory

```bash
$ php artisan tenancy:installer
```

### Migrating/seeding

#### Central/Landlord

```bash
$ php artisan migrate [--seed]
```

#### Tenants

[stancl/tenancy console commands](https://tenancyforlaravel.com/docs/v3/console-commands)

Edit `config/tenancy.php` `migration_parameters` and `seeder_parameters`

```bash
$ php artisan tenants:migrate [--tenants=TENANT_ID]
$ php artisan tenants:seed [--tenants=TENANT_ID] 
$ php artisan tenants:run role:set --argument="user_id=12345"
```

#### Splitting routes/verifying a tenant early

You can use a `Preloader` to bring back the tenant without initializing tenancy.

In the example below, the `DomainPreloader` is used in conjunction with the
`InitializeTenancyByDomain` middleware (to actually initialize tenancy).

`app/Providers/RouteServiceProvider.php`

```php
public function boot(): void
{
    $this->configureRateLimiting();

    $this->routes(function () {
        $tenant = $this->preloadTenant();
        if (!$tenant) {
            Route::prefix('api')
                 ->middleware('api')
                 ->namespace($this->namespace)
                 ->group(base_path('routes/api.php'));
            Route::middleware('web')
                 ->namespace($this->namespace)
                 ->group(base_path('routes/web.php'));
        } else {
            Route::namespace($this->namespace)
                 ->middleware([
                     'web',
                     InitializeTenancyByDomain::class,
                     PreventAccessFromCentralDomains::class,
                 ])
                 ->group(base_path('routes/tenant.php'));
        }
    });
}

protected function preloadTenant(): ?Tenant
    {
        if ($this->app->runningInConsole()) {
            return null;
        }
        /** @var \Smorken\Tenancy\Contracts\Preloaders\Preloader $loader */
        $loader = $this->app->make(DomainPreloader::class);
        return $loader->load($this->app['request']);
    }
```
