<?php
return [
    'init_middleware' => \Stancl\Tenancy\Middleware\InitializeTenancyByDomain::class,
    'cache_prefix' => 'tenant',
    'assets' => [
        'load_routes' => env('TENANCY_ASSETS_LOAD_ROUTES', true),
        'controller' => env('TENANCY_ASSET_CONTROLLER', \Smorken\Tenancy\Controllers\Asset\Controller::class),
    ],
    'admin' => [
        'load_routes' => env('TENANCY_ADMIN_LOAD_ROUTES', true),
        'prefix' => env('TENANCY_ADMIN_PREFIX', 'admin'),
        'middleware' => ['web', 'auth', 'can:role-admin'],
        'layout' => 'layouts.app',
    ],
    /**
     * Base path (off of laravel base path) for tenants directory.
     * Contains tenant config files and resources
     * "core" directories are common, "tenant_{tenantId}" is per tenant
     */
    'tenants_base' => env('TENANCY_TENANTS_BASE', 'tenants'),
    'tenants_use_central_views' => env('TENANCY_USE_CENTRAL_VIEWS', false),
    /**
     * Array of connection keys and config arrays to configure (not for tenant/default)
     * env keys will be CONNECTIONNAME_TENANTID_ENVKEY
     * setting the config array to null or empty will use the default configuration options of
     * ['HOST' => 'host', 'DATABASE' => 'database', 'USERNAME' => 'username', 'PASSWORD' => 'password']
     * Example
     * 'connectionName' => ['OTHERKEY' => 'somethingelse', 'HOST' => 'host', ...],
     * 'connectionName2' => null,
     */
    'connections' => [
    ],
    /**
     * Configs that will be completely overwritten by the tenant and not merged with
     * the landlord
     */
    'overwrite_config_keys' => [
        'menus',
    ],
    'invokable_services' => [
        'invokables' => [
            \Smorken\Tenancy\Providers\Services\Admin\TenantServices::class,
            \Smorken\Tenancy\Providers\Services\TenantServices::class,
        ],
    ],
    'providers' => [
        'concrete' => [
            \Smorken\Tenancy\Storage\Eloquent\Tenant::class => [
                'model' => [
                    'impl' => \Smorken\Tenancy\Models\Eloquent\Tenant::class,
                ],
                'cacheAssistOptions' => [
                    'forgetAuto' => [['active']],
                ],
            ],
        ],
        'contract' => [
            \Smorken\Tenancy\Contracts\Storage\Tenant::class => \Smorken\Tenancy\Storage\Eloquent\Tenant::class,
        ],
    ],
];
